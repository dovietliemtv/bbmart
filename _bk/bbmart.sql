-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 27, 2019 at 05:44 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bbmart`
--

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_general_options`
--

CREATE TABLE `alipo_cms_general_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `footer_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_home_pages`
--

CREATE TABLE `alipo_cms_home_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_media_pages`
--

CREATE TABLE `alipo_cms_media_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_neighbourhood_pages`
--

CREATE TABLE `alipo_cms_neighbourhood_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_generaloption_contact_infos`
--

CREATE TABLE `alipo_generaloption_contact_infos` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_generaloption_generals`
--

CREATE TABLE `alipo_generaloption_generals` (
  `id` int(10) UNSIGNED NOT NULL,
  `title1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `des1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_network` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `des2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `copyright` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_generaloption_generals`
--

INSERT INTO `alipo_generaloption_generals` (`id`, `title1`, `des1`, `phone`, `social_network`, `title2`, `des2`, `copyright`, `created_at`, `updated_at`) VALUES
(1, 'STELLA MEGA CITY', '<p class=\"ct-foot-info ct-address\">Đường Đặng Văn Dầy, Q. Bình Thuỷ, TP.Cần Thơ</p>\r\n\r\n<p class=\"ct-foot-info ct-web\">stella-megacity.com</p>\r\n\r\n<p class=\"ct-foot-info ct-phone\"><a href=\"tel:+84908885888\">090 888 5 888</a></p>', '090 888 5 888', '[{\"link\":\"#\"},{\"link\":\"#\"},{\"link\":\"#\"}]', 'CÔNG TY CỔ PHẦN KITA INVEST', '<p class=\"ct-foot-info ct-address\">27 Lê Quý Đôn, P. 7, Q. 3, TP. HCM</p>\r\n\r\n<p class=\"ct-foot-info ct-web\">kita-group.com</p>\r\n\r\n<p class=\"ct-foot-info ct-fax\"><a href=\"tel:+842839302855\">(028) 3930 2855</a></p>', 'Copyright © 2019 by KITA Group', '2019-07-08 07:02:28', '2019-09-30 02:58:20');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_page_galleries`
--

CREATE TABLE `alipo_page_galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `videos` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_page_galleries`
--

INSERT INTO `alipo_page_galleries` (`id`, `title`, `slug`, `images`, `videos`, `created_at`, `updated_at`) VALUES
(1, 'Thư viện', 'thu-vien', '[{\"image\":\"\\/Hinh anh\\/1.jpg\"},{\"image\":\"\\/Hinh anh\\/13-1.jpg\"},{\"image\":\"\\/Hinh anh\\/18-1.jpg\"},{\"image\":\"\\/Hinh anh\\/2.jpg\"},{\"image\":\"\\/Hinh anh\\/3.jpg\"},{\"image\":\"\\/Hinh anh\\/4-1.jpg\"},{\"image\":\"\\/Hinh anh\\/4.jpg\"},{\"image\":\"\\/Hinh anh\\/61b1f7d3b5bd52e30bac.jpg\"},{\"image\":\"\\/Hinh anh\\/712bff74bd1a5a44030b-1.jpg\"},{\"image\":\"\\/Hinh anh\\/90ef9eb2dcdc3b8262cd.jpg\"},{\"image\":\"\\/Hinh anh\\/dd120472461ca142f80d.jpg\"},{\"image\":\"\\/Hinh anh\\/pn25-1.jpg\"}]', '[{\"image\":\"\\/23.png\",\"youtube_id\":\"-cvYXIYotsA\"}]', '2019-08-24 12:02:05', '2019-10-27 02:52:35');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_page_homes`
--

CREATE TABLE `alipo_page_homes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `introduction_des` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `facilities_des` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lydoluachon` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_house_dinfo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_page_homes`
--

INSERT INTO `alipo_page_homes` (`id`, `title`, `slug`, `background`, `introduction_des`, `facilities_des`, `lydoluachon`, `type_house_dinfo`, `created_at`, `updated_at`) VALUES
(1, 'Home', 'home', 'image', '<p>Hòa mình trong không gian thiên nhiên với mặt tiền là dòng sông Hậu trải dài, Stella Mega City là khu đô thị hiện đại với không khí trong lành tươi mát quanh năm và thiên nhiên là khởi nguồn cho mọi sức sống. Với tầm nhìn đưa Stella Mega City trở thành một đại đô thị năng động, hiện đại – trung tâm kinh tế tại khu vực phía Tây Bắc thành phố Cần Thơ, Stella Mega City tự hào mang trong mình sứ mệnh đem đến cuộc sống chất lượng, đẳng cấp hơn cùng những giá trị thực sự khác biệt.</p>', '<p>Stella Mega City là dự án đại đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế như: trường học, sân vận động, trung tâm triển lãm hội nghị đa chức năng, trung tâm thương mại – dịch vụ, khu liên hợp thể thao, trung tâm sự kiện… Stella Mega City được đầu tư hạ tầng nội khu hoàn hảo với đường nội khu trải nhựa lộ giới 6m, vỉa hè cây xanh, đèn điện chiếu sáng, cảnh quan môi trường sống được chú trọng nhằm mang đến không gian tươi mát, tầm nhìn đẹp và rộng mở cho cộng đồng dân cư sinh sống tại đây.</p>', '[{\"image\":\"\\/tinhhoa-2.png\",\"text\":\"\\u0110\\u00f4i ng\\u0169 thi\\u1ebft k\\u1ebf t\\u1eeb \\u00dd\"},{\"image\":\"\\/nhamay4-2.png\",\"text\":\"Nh\\u00e0 m\\u00e1y 4.0  hi\\u1ec7n \\u0111\\u1ea1i, b\\u1eadc nh\\u1ea5t \\u0110\\u00f4ng Nam \\u00c1\"},{\"image\":\"\\/EU-2.png\",\"text\":\"Ngu\\u1ed3n nguy\\u00ean v\\u1eadt li\\u1ec7u v\\u00e0 linh ki\\u1ec7n Ch\\u00e2u \\u00c2u\"},{\"image\":\"\\/E1-2.png\",\"text\":\"100% v\\u1eadt li\\u1ec7u s\\u1ea1ch, an to\\u00e0n cho s\\u1ee9c kho\\u1ebb\"},{\"image\":\"\\/dadangmauma-2.png\",\"text\":\"10.000 c\\u00e1ch ph\\u1ed1i v\\u00e0 gi\\u1ea3i ph\\u00e1p m\\u00e0u s\\u1eafc\"},{\"image\":\"\\/thietkethongminh-2.png\",\"text\":\"Thi\\u1ebft k\\u1ebf th\\u00f4ng minh, ti\\u1ebft ki\\u1ec7m kh\\u00f4ng gian s\\u1ed1ng\"}]', '[]', '2019-07-08 06:32:47', '2019-10-27 02:45:49');

-- --------------------------------------------------------

--
-- Table structure for table `backend_access_log`
--

CREATE TABLE `backend_access_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_access_log`
--

INSERT INTO `backend_access_log` (`id`, `user_id`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, 1, '::1', '2019-07-07 10:08:14', '2019-07-07 10:08:14'),
(2, 1, '115.77.187.74', '2019-07-09 22:08:20', '2019-07-09 22:08:20'),
(3, 1, '118.71.170.224', '2019-07-09 22:37:42', '2019-07-09 22:37:42'),
(4, 1, '118.71.170.224', '2019-07-10 01:51:02', '2019-07-10 01:51:02'),
(5, 1, '118.71.170.224', '2019-07-10 02:03:12', '2019-07-10 02:03:12'),
(6, 1, '171.235.143.123', '2019-07-13 23:35:38', '2019-07-13 23:35:38'),
(7, 1, '171.235.143.123', '2019-07-14 00:13:52', '2019-07-14 00:13:52'),
(8, 1, '171.235.143.123', '2019-07-14 00:17:14', '2019-07-14 00:17:14'),
(9, 1, '171.235.143.123', '2019-07-14 00:17:56', '2019-07-14 00:17:56'),
(10, 1, '171.235.143.123', '2019-07-14 00:18:24', '2019-07-14 00:18:24'),
(11, 1, '118.71.170.225', '2019-07-14 21:17:24', '2019-07-14 21:17:24'),
(12, 1, '116.109.32.79', '2019-07-15 17:35:55', '2019-07-15 17:35:55'),
(13, 1, '116.109.32.79', '2019-07-15 17:37:31', '2019-07-15 17:37:31'),
(14, 1, '1.53.114.49', '2019-07-17 01:30:43', '2019-07-17 01:30:43'),
(15, 1, '118.68.123.21', '2019-07-22 04:19:29', '2019-07-22 04:19:29'),
(16, 1, '118.68.123.21', '2019-07-22 04:20:07', '2019-07-22 04:20:07'),
(17, 1, '1.53.114.48', '2019-07-23 01:29:11', '2019-07-23 01:29:11'),
(18, 1, '::1', '2019-07-27 21:44:52', '2019-07-27 21:44:52'),
(19, 1, '116.109.17.28', '2019-07-28 07:09:57', '2019-07-28 07:09:57'),
(20, 1, '1.53.160.151', '2019-08-10 10:10:51', '2019-08-10 10:10:51'),
(21, 1, '113.176.62.219', '2019-08-11 19:15:38', '2019-08-11 19:15:38'),
(22, 1, '::1', '2019-08-23 20:45:37', '2019-08-23 20:45:37'),
(23, 1, '::1', '2019-09-01 23:27:45', '2019-09-01 23:27:45'),
(24, 1, '::1', '2019-09-02 09:59:16', '2019-09-02 09:59:16'),
(25, 1, '::1', '2019-09-02 10:01:12', '2019-09-02 10:01:12'),
(26, 2, '::1', '2019-09-02 10:04:47', '2019-09-02 10:04:47'),
(27, 1, '::1', '2019-09-02 10:05:24', '2019-09-02 10:05:24'),
(28, 2, '::1', '2019-09-02 10:08:23', '2019-09-02 10:08:23'),
(29, 1, '::1', '2019-09-02 10:08:48', '2019-09-02 10:08:48'),
(30, 2, '::1', '2019-09-02 10:10:00', '2019-09-02 10:10:00'),
(31, 2, '27.78.224.18', '2019-09-02 10:35:21', '2019-09-02 10:35:21'),
(32, 1, '118.71.170.240', '2019-09-02 22:04:22', '2019-09-02 22:04:22'),
(33, 2, '118.71.170.240', '2019-09-02 22:07:18', '2019-09-02 22:07:18'),
(34, 2, '113.176.62.219', '2019-09-02 23:55:14', '2019-09-02 23:55:14'),
(35, 1, '118.71.170.240', '2019-09-02 23:58:51', '2019-09-02 23:58:51'),
(36, 1, '118.71.170.240', '2019-09-03 00:00:47', '2019-09-03 00:00:47'),
(37, 1, '113.176.62.219', '2019-09-03 00:11:23', '2019-09-03 00:11:23'),
(38, 1, '113.176.62.219', '2019-09-04 19:07:41', '2019-09-04 19:07:41'),
(39, 1, '113.176.62.219', '2019-09-06 03:02:55', '2019-09-06 03:02:55'),
(40, 2, '14.234.78.107', '2019-09-06 08:35:18', '2019-09-06 08:35:18'),
(41, 1, '14.234.78.107', '2019-09-06 09:11:05', '2019-09-06 09:11:05'),
(42, 1, '::1', '2019-09-11 08:44:54', '2019-09-11 08:44:54'),
(43, 2, '103.199.57.114', '2019-09-16 17:25:49', '2019-09-16 17:25:49'),
(44, 1, '::1', '2019-09-18 16:07:15', '2019-09-18 16:07:15'),
(45, 1, '115.77.187.74', '2019-09-18 23:00:42', '2019-09-18 23:00:42'),
(46, 1, '115.73.213.13', '2019-09-21 22:08:20', '2019-09-21 22:08:20'),
(47, 1, '103.199.56.229', '2019-09-21 22:21:21', '2019-09-21 22:21:21'),
(48, 1, '116.102.17.183', '2019-09-22 08:04:14', '2019-09-22 08:04:14'),
(49, 1, '116.102.17.183', '2019-09-22 08:07:49', '2019-09-22 08:07:49'),
(50, 2, '118.68.62.95', '2019-09-22 11:22:08', '2019-09-22 11:22:08'),
(51, 1, '118.68.62.95', '2019-09-22 11:22:22', '2019-09-22 11:22:22'),
(52, 1, '118.71.170.231', '2019-09-22 19:47:24', '2019-09-22 19:47:24'),
(53, 1, '115.77.187.74', '2019-09-22 21:03:56', '2019-09-22 21:03:56'),
(54, 1, '113.176.62.219', '2019-09-25 18:45:11', '2019-09-25 18:45:11'),
(55, 1, '1.53.114.151', '2019-09-25 19:22:53', '2019-09-25 19:22:53'),
(56, 1, '113.176.62.219', '2019-09-25 19:29:04', '2019-09-25 19:29:04'),
(57, 1, '1.53.114.151', '2019-09-25 21:30:09', '2019-09-25 21:30:09'),
(58, 1, '42.116.118.224', '2019-09-28 03:43:41', '2019-09-28 03:43:41'),
(59, 1, '103.199.33.107', '2019-09-30 02:33:21', '2019-09-30 02:33:21'),
(60, 1, '113.176.62.219', '2019-09-30 03:26:59', '2019-09-30 03:26:59'),
(61, 1, '42.117.235.29', '2019-09-30 06:27:31', '2019-09-30 06:27:31'),
(62, 1, '14.246.3.22', '2019-10-26 21:35:51', '2019-10-26 21:35:51'),
(63, 1, '1.54.23.207', '2019-10-27 02:41:07', '2019-10-27 02:41:07');

-- --------------------------------------------------------

--
-- Table structure for table `backend_users`
--

CREATE TABLE `backend_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users`
--

INSERT INTO `backend_users` (`id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `role_id`, `activated_at`, `last_login`, `created_at`, `updated_at`, `deleted_at`, `is_superuser`) VALUES
(1, 'Admin', 'Person', 'admin', 'admin@domain.tld', '$2y$10$3JN.su6hOr4zbQS6R2uYXOFenRcoez.GKUcyXvbiTiTsOmapxOOLu', NULL, '$2y$10$SksMofB81pUUfqkm6cq3kOeQ6RL9zwf8GodEjc.aws.boCxEPBHKK', NULL, '', 1, 2, NULL, '2019-10-27 02:41:07', '2019-07-07 10:08:09', '2019-10-27 02:41:07', NULL, 1),
(2, 'Content', 'Role', 'content', 'admin@domain.co', '$2y$10$luUC/q83dDkoFYDKMT31WupaC6fwKdfMALwCmNMMdlIDlZX5NXpt2', NULL, '$2y$10$AcsFB/3PeRQXQcd0vJHr4uoMFO2Vd6eHK0zgyKiePK1mhQuJYBlSG', NULL, '{\"cms.manage_content\":-1,\"cms.manage_theme_options\":-1,\"cms.manage_assets\":-1,\"cms.manage_pages\":-1,\"cms.manage_layouts\":-1,\"cms.manage_partials\":-1,\"cms.manage_themes\":-1,\"anandpatel.wysiwygeditors.settings\":-1,\"system.manage_mail_templates\":-1,\"backend.access_dashboard\":-1,\"system.manage_mail_settings\":-1,\"system.access_logs\":-1,\"system.manage_updates\":-1,\"backend.manage_default_dashboard\":-1,\"backend.manage_branding\":-1,\"backend.manage_editor\":-1,\"backend.manage_preferences\":-1,\"backend.impersonate_users\":-1,\"backend.manage_users\":-1,\"laminsanneh.flexicontact.access_settings\":-1,\"benfreke.menumanager.access_menumanager\":1}', 0, 1, NULL, '2019-09-22 11:22:08', '2019-09-02 10:04:35', '2019-09-22 11:22:08', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `backend_users_groups`
--

CREATE TABLE `backend_users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users_groups`
--

INSERT INTO `backend_users_groups` (`user_id`, `user_group_id`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_groups`
--

CREATE TABLE `backend_user_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_groups`
--

INSERT INTO `backend_user_groups` (`id`, `name`, `created_at`, `updated_at`, `code`, `description`, `is_new_user_default`) VALUES
(1, 'Owners', '2019-07-07 10:08:09', '2019-07-07 10:08:09', 'owners', 'Default group for website owners.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_preferences`
--

CREATE TABLE `backend_user_preferences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_roles`
--

CREATE TABLE `backend_user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_roles`
--

INSERT INTO `backend_user_roles` (`id`, `name`, `code`, `description`, `permissions`, `is_system`, `created_at`, `updated_at`) VALUES
(1, 'Publisher', 'publisher', 'Site editor with access to publishing tools.', '', 1, '2019-07-07 10:08:09', '2019-07-07 10:08:09'),
(2, 'Developer', 'developer', 'Site administrator with access to developer tools.', '', 1, '2019-07-07 10:08:09', '2019-07-07 10:08:09');

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_throttle`
--

CREATE TABLE `backend_user_throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_throttle`
--

INSERT INTO `backend_user_throttle` (`id`, `user_id`, `ip_address`, `attempts`, `last_attempt_at`, `is_suspended`, `suspended_at`, `is_banned`, `banned_at`) VALUES
(1, 1, '::1', 0, NULL, 0, NULL, 0, NULL),
(2, 1, '115.77.187.74', 0, NULL, 0, NULL, 0, NULL),
(3, 1, '118.71.170.224', 0, NULL, 0, NULL, 0, NULL),
(4, 1, '113.23.87.230', 0, NULL, 0, NULL, 0, NULL),
(5, 1, '27.78.230.97', 0, NULL, 0, NULL, 0, NULL),
(6, 1, '118.71.170.218', 0, NULL, 0, NULL, 0, NULL),
(7, 1, '116.109.77.165', 0, NULL, 0, NULL, 0, NULL),
(8, 1, '118.71.170.221', 0, NULL, 0, NULL, 0, NULL),
(9, 1, '171.235.143.123', 0, NULL, 0, NULL, 0, NULL),
(10, 1, '118.71.170.225', 0, NULL, 0, NULL, 0, NULL),
(11, 1, '116.109.32.79', 0, NULL, 0, NULL, 0, NULL),
(12, 1, '1.53.114.49', 0, NULL, 0, NULL, 0, NULL),
(13, 1, '118.68.123.21', 0, NULL, 0, NULL, 0, NULL),
(14, 1, '113.185.77.57', 0, NULL, 0, NULL, 0, NULL),
(15, 1, '1.53.114.48', 0, NULL, 0, NULL, 0, NULL),
(16, 1, '116.109.17.28', 0, NULL, 0, NULL, 0, NULL),
(17, 1, '1.53.114.61', 0, NULL, 0, NULL, 0, NULL),
(18, 1, '1.53.114.107', 0, NULL, 0, NULL, 0, NULL),
(19, 1, '113.185.75.65', 0, NULL, 0, NULL, 0, NULL),
(20, 1, '42.118.232.127', 0, NULL, 0, NULL, 0, NULL),
(21, 1, '1.53.114.106', 0, NULL, 0, NULL, 0, NULL),
(22, 1, '1.53.160.151', 0, NULL, 0, NULL, 0, NULL),
(23, 1, '113.176.62.219', 0, NULL, 0, NULL, 0, NULL),
(24, 1, '116.109.183.94', 0, NULL, 0, NULL, 0, NULL),
(25, 2, '::1', 0, NULL, 0, NULL, 0, NULL),
(26, 2, '27.78.224.18', 0, NULL, 0, NULL, 0, NULL),
(27, 1, '118.71.170.240', 0, NULL, 0, NULL, 0, NULL),
(28, 2, '118.71.170.240', 0, NULL, 0, NULL, 0, NULL),
(29, 2, '113.176.62.219', 0, NULL, 0, NULL, 0, NULL),
(30, 1, '118.71.170.244', 0, NULL, 0, NULL, 0, NULL),
(31, 2, '14.234.78.107', 0, NULL, 0, NULL, 0, NULL),
(32, 1, '14.234.78.107', 0, NULL, 0, NULL, 0, NULL),
(33, 1, '58.187.175.226', 0, NULL, 0, NULL, 0, NULL),
(34, 1, '115.79.227.159', 0, NULL, 0, NULL, 0, NULL),
(35, 1, '118.71.170.64', 0, NULL, 0, NULL, 0, NULL),
(36, 1, '1.53.114.129', 0, NULL, 0, NULL, 0, NULL),
(37, 1, '116.109.14.19', 0, NULL, 0, NULL, 0, NULL),
(38, 2, '103.199.57.114', 0, NULL, 0, NULL, 0, NULL),
(39, 1, '103.199.57.114', 0, NULL, 0, NULL, 0, NULL),
(40, 1, '1.53.114.211', 0, NULL, 0, NULL, 0, NULL),
(41, 1, '118.71.170.232', 0, NULL, 0, NULL, 0, NULL),
(42, 1, '116.102.17.183', 0, NULL, 0, NULL, 0, NULL),
(43, 1, '118.71.170.227', 0, NULL, 0, NULL, 0, NULL),
(44, 1, '115.73.213.13', 0, NULL, 0, NULL, 0, NULL),
(45, 1, '103.199.56.229', 0, NULL, 0, NULL, 0, NULL),
(46, 2, '118.68.62.95', 0, NULL, 0, NULL, 0, NULL),
(47, 1, '118.68.62.95', 0, NULL, 0, NULL, 0, NULL),
(48, 1, '118.71.170.231', 0, NULL, 0, NULL, 0, NULL),
(49, 1, '1.53.114.151', 0, NULL, 0, NULL, 0, NULL),
(50, 1, '42.116.118.224', 0, NULL, 0, NULL, 0, NULL),
(51, 1, '118.71.170.79', 0, NULL, 0, NULL, 0, NULL),
(52, 1, '103.199.33.107', 0, NULL, 0, NULL, 0, NULL),
(53, 1, '42.117.235.29', 0, NULL, 0, NULL, 0, NULL),
(54, 1, '171.233.182.189', 0, NULL, 0, NULL, 0, NULL),
(55, 1, '118.71.170.73', 0, NULL, 0, NULL, 0, NULL),
(56, 1, '42.112.185.175', 0, NULL, 0, NULL, 0, NULL),
(57, 1, '14.246.3.22', 0, NULL, 0, NULL, 0, NULL),
(58, 1, '1.54.23.207', 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_data`
--

CREATE TABLE `cms_theme_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_logs`
--

CREATE TABLE `cms_theme_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `old_content` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_templates`
--

CREATE TABLE `cms_theme_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `source` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(10) UNSIGNED NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `deferred_bindings`
--

CREATE TABLE `deferred_bindings` (
  `id` int(10) UNSIGNED NOT NULL,
  `master_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `deferred_bindings`
--

INSERT INTO `deferred_bindings` (`id`, `master_type`, `master_field`, `slave_type`, `slave_id`, `session_key`, `is_bind`, `created_at`, `updated_at`) VALUES
(217, 'Alipo\\Page\\Models\\Home', 'banner', 'System\\Models\\File', '220', '5uONDVdAesiF9u3AqIKsBCnG71s4wrcTm1IsOlWJ', 1, '2019-09-06 09:22:50', '2019-09-06 09:22:50'),
(218, 'Alipo\\Page\\Models\\Home', 'banner', 'System\\Models\\File', '219', '5uONDVdAesiF9u3AqIKsBCnG71s4wrcTm1IsOlWJ', 1, '2019-09-06 09:22:50', '2019-09-06 09:22:50');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci,
  `failed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_10_01_000001_Db_Deferred_Bindings', 1),
(2, '2013_10_01_000002_Db_System_Files', 1),
(3, '2013_10_01_000003_Db_System_Plugin_Versions', 1),
(4, '2013_10_01_000004_Db_System_Plugin_History', 1),
(5, '2013_10_01_000005_Db_System_Settings', 1),
(6, '2013_10_01_000006_Db_System_Parameters', 1),
(7, '2013_10_01_000007_Db_System_Add_Disabled_Flag', 1),
(8, '2013_10_01_000008_Db_System_Mail_Templates', 1),
(9, '2013_10_01_000009_Db_System_Mail_Layouts', 1),
(10, '2014_10_01_000010_Db_Jobs', 1),
(11, '2014_10_01_000011_Db_System_Event_Logs', 1),
(12, '2014_10_01_000012_Db_System_Request_Logs', 1),
(13, '2014_10_01_000013_Db_System_Sessions', 1),
(14, '2015_10_01_000014_Db_System_Mail_Layout_Rename', 1),
(15, '2015_10_01_000015_Db_System_Add_Frozen_Flag', 1),
(16, '2015_10_01_000016_Db_Cache', 1),
(17, '2015_10_01_000017_Db_System_Revisions', 1),
(18, '2015_10_01_000018_Db_FailedJobs', 1),
(19, '2016_10_01_000019_Db_System_Plugin_History_Detail_Text', 1),
(20, '2016_10_01_000020_Db_System_Timestamp_Fix', 1),
(21, '2017_08_04_121309_Db_Deferred_Bindings_Add_Index_Session', 1),
(22, '2017_10_01_000021_Db_System_Sessions_Update', 1),
(23, '2017_10_01_000022_Db_Jobs_FailedJobs_Update', 1),
(24, '2017_10_01_000023_Db_System_Mail_Partials', 1),
(25, '2017_10_23_000024_Db_System_Mail_Layouts_Add_Options_Field', 1),
(26, '2013_10_01_000001_Db_Backend_Users', 2),
(27, '2013_10_01_000002_Db_Backend_User_Groups', 2),
(28, '2013_10_01_000003_Db_Backend_Users_Groups', 2),
(29, '2013_10_01_000004_Db_Backend_User_Throttle', 2),
(30, '2014_01_04_000005_Db_Backend_User_Preferences', 2),
(31, '2014_10_01_000006_Db_Backend_Access_Log', 2),
(32, '2014_10_01_000007_Db_Backend_Add_Description_Field', 2),
(33, '2015_10_01_000008_Db_Backend_Add_Superuser_Flag', 2),
(34, '2016_10_01_000009_Db_Backend_Timestamp_Fix', 2),
(35, '2017_10_01_000010_Db_Backend_User_Roles', 2),
(36, '2018_12_16_000011_Db_Backend_Add_Deleted_At', 2),
(37, '2014_10_01_000001_Db_Cms_Theme_Data', 3),
(38, '2016_10_01_000002_Db_Cms_Timestamp_Fix', 3),
(39, '2017_10_01_000003_Db_Cms_Theme_Logs', 3),
(40, '2018_11_01_000001_Db_Cms_Theme_Templates', 3);

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_attributes`
--

CREATE TABLE `rainlab_translate_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_data` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_indexes`
--

CREATE TABLE `rainlab_translate_indexes` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_locales`
--

CREATE TABLE `rainlab_translate_locales` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rainlab_translate_locales`
--

INSERT INTO `rainlab_translate_locales` (`id`, `code`, `name`, `is_default`, `is_enabled`, `sort_order`) VALUES
(1, 'en', 'English', 0, 1, 1),
(2, 'vi', 'Tiếng Việt', 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_messages`
--

CREATE TABLE `rainlab_translate_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_data` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rainlab_translate_messages`
--

INSERT INTO `rainlab_translate_messages` (`id`, `code`, `message_data`) VALUES
(85, 'đón.tết.tài.lộ', '{\"x\":\"\\u0110\\u00f3n t\\u1ebft t\\u00e0i l\\u1ed9\"}'),
(86, 'đổi.bếp.mới', '{\"x\":\"\\u0110\\u1ed5i b\\u1ebfp m\\u1edbi\"}'),
(87, 'tặng.bộ.phụ.kiện', '{\"x\":\"T\\u1eb7ng b\\u1ed9 ph\\u1ee5 ki\\u1ec7n\"}'),
(88, 'trị.giá', '{\"x\":\"Tr\\u1ecb gi\\u00e1\"}'),
(89, 'nhận.ngay.ưu.đã', '{\"x\":\"NH\\u1eacN NGAY \\u01afU \\u0110\\u00c3\"}'),
(90, 'lý.do.chọn', '{\"x\":\"L\\u00dd DO CH\\u1eccN\"}'),
(91, 'bb.smart.decory', '{\"x\":\"BB SMART DECORY\"}'),
(92, 'bb.smart.decor', '{\"x\":\"BB SMART DECOR\"}'),
(93, 'gói.5.free', '{\"x\":\"G\\u00f3i 5 Free\"}'),
(94, 'trị.giá.lên.đến', '{\"x\":\"tr\\u1ecb gi\\u00e1 l\\u00ean \\u0111\\u1ebfn\"}'),
(95, '100.triệu.đồn', '{\"x\":\"100 tri\\u1ec7u \\u0111\\u1ed3n\"}'),
(96, 'free.tư.vấn', '{\"x\":\"FREE T\\u01b0 v\\u1ea5n\"}'),
(97, 'free.khảo.sát', '{\"x\":\"FREE Kh\\u1ea3o s\\u00e1t\"}'),
(98, 'free.thiết.kế.3d', '{\"x\":\"FREE Thi\\u1ebft k\\u1ebf 3D\"}'),
(99, 'free.giao.hàng', '{\"x\":\"FREE Giao h\\u00e0ng\"}'),
(100, 'free.lắp.đặt', '{\"x\":\"FREE L\\u1eafp \\u0111\\u1eb7t\"}'),
(101, 'mẫu.thiết.kế.bb.smart.decor', '{\"x\":\"M\\u1eaaU THI\\u1ebeT K\\u1ebe BB SMART DECOR\"}'),
(102, 'hình.ảnh', '{\"x\":\"H\\u00ecnh \\u1ea3nh\"}'),
(103, 'video', '{\"x\":\"Video\"}'),
(104, 'hotline', '{\"x\":\"Hotline\"}'),
(105, 'showroom.85.87.nguyễn.cơ.thạch.khu.đô.thị.sala.p.an.lợi.đông.q.2.tp.hồ.chí.minh', '{\"x\":\"Showroom: 85-87 Nguy\\u1ec5n C\\u01a1 Th\\u1ea1ch, khu \\u0111\\u00f4 th\\u1ecb Sala,\\n                    P. An L\\u1ee3i \\u0110\\u00f4ng, Q.2, Tp. H\\u1ed3 Ch\\u00ed Minh\"}'),
(106, 'họ.tên', '{\"x\":\"H\\u1ecd t\\u00ean\"}'),
(107, 'đầy.đủ.tên.của.bạn', '{\"x\":\"\\u0110\\u1ea7y \\u0111\\u1ee7 t\\u00ean c\\u1ee7a b\\u1ea1n\"}'),
(108, 'số.điện.thoại', '{\"x\":\"S\\u1ed1 \\u0111i\\u1ec7n tho\\u1ea1i\"}'),
(109, 'đăng.ký', '{\"x\":\"\\u0110\\u0103ng k\\u00fd\"}'),
(110, '85.87.nguyễn.cơ.thạch.khu.đô.thị.sala.p.an.lợi.đông.q2', '{\"x\":\"85-87 Nguy\\u1ec5n C\\u01a1 Th\\u1ea1ch, khu \\u0111\\u00f4 th\\u1ecb Sala,\\n                    P. An L\\u1ee3i \\u0110\\u00f4ng, Q2\"}'),
(111, 'copyright.2019.bb.smart.decor', '{\"x\":\"copyright 2019 @ BB Smart Decor\"}'),
(112, 'sỡ.hữu.gói.nội.thất.chuẩn.châu.âu', '{\"x\":\"S\\u1ee1 h\\u1eefu g\\u00f3i n\\u1ed9i th\\u1ea5t chu\\u1ea9n Ch\\u00e2u \\u00c2u\"}'),
(113, 'với.giá.0đ', '{\"x\":\"v\\u1edbi gi\\u00e1 0\\u0111\"}'),
(114, 'đăng.ký.tư.vấn', '{\"x\":\"\\u0110\\u0103ng k\\u00fd t\\u01b0 v\\u1ea5n\"}'),
(115, 'vi', '{\"x\":\"Vi\"}'),
(116, 'en', '{\"x\":\"En\"}'),
(117, 'email', '{\"x\":\"email\"}'),
(118, 'thông.tin.đã.được.gửi.thành.công', '{\"x\":\"Th\\u00f4ng tin \\u0111\\u00e3 \\u0111\\u01b0\\u1ee3c g\\u1eedi th\\u00e0nh c\\u00f4ng\"}'),
(119, 'cám.ơn.quý.khách.đã.liên.hệ', '{\"x\":\"C\\u00e1m \\u01a1n qu\\u00fd kh\\u00e1ch \\u0111\\u00e3 li\\u00ean h\\u1ec7\"}');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci,
  `last_activity` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_event_logs`
--

CREATE TABLE `system_event_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `details` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_event_logs`
--

INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(1, 'error', 'Swift_RfcComplianceException: Address in mailbox given [] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(NULL, \'liemdo11\')\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(NULL, \'liemdo11\', \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(NULL, \'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:07:21', '2019-10-27 09:07:21'),
(2, 'error', 'Swift_RfcComplianceException: Address in mailbox given [] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(NULL, \'liemdo11\')\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(NULL, \'liemdo11\', \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(NULL, \'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:10:19', '2019-10-27 09:10:19'),
(3, 'error', 'Swift_RfcComplianceException: Address in mailbox given [] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(NULL, \'liemdo11\')\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(NULL, \'liemdo11\', \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(NULL, \'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:10:28', '2019-10-27 09:10:28'),
(4, 'error', 'Swift_RfcComplianceException: Address in mailbox given [] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(NULL, \'liemdo11\')\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(NULL, \'liemdo11\', \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(NULL, \'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:13:56', '2019-10-27 09:13:56');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(5, 'error', 'Swift_RfcComplianceException: Address in mailbox given [] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(NULL, \'liemdo11\')\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(NULL, \'liemdo11\', \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(NULL, \'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:14:30', '2019-10-27 09:14:30'),
(6, 'error', 'Swift_RfcComplianceException: Address in mailbox given [] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(NULL, \'liemdo11\')\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(NULL, \'liemdo11\', \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(NULL, \'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:15:01', '2019-10-27 09:15:01'),
(7, 'error', 'Swift_RfcComplianceException: Address in mailbox given [] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(NULL, \'liemdo11\')\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(NULL, \'liemdo11\', \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(NULL, \'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:15:07', '2019-10-27 09:15:07'),
(8, 'error', 'Swift_RfcComplianceException: Address in mailbox given [] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(NULL, \'liemdo11\')\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(NULL, \'liemdo11\', \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(NULL, \'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:16:17', '2019-10-27 09:16:17');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(9, 'error', 'Swift_RfcComplianceException: Address in mailbox given [] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(NULL, \'liemdo11\')\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(NULL, \'liemdo11\', \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(NULL, \'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:19:18', '2019-10-27 09:19:18'),
(10, 'error', 'Swift_RfcComplianceException: Address in mailbox given [] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(NULL, \'liemdo11\')\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(NULL, \'liemdo11\', \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(NULL, \'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:19:44', '2019-10-27 09:19:44'),
(11, 'error', 'Swift_RfcComplianceException: Address in mailbox given [] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(NULL, \'liemdo11\')\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(NULL, \'liemdo11\', \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(NULL, \'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:20:45', '2019-10-27 09:20:45'),
(12, 'error', 'Swift_RfcComplianceException: Address in mailbox given [liemdo11] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'liemdo11\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(\'liemdo11\', NULL)\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(\'liemdo11\', NULL, \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(\'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:21:18', '2019-10-27 09:21:18');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(13, 'error', 'Swift_RfcComplianceException: Address in mailbox given [liemdo11] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'liemdo11\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(\'liemdo11\', NULL)\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(\'liemdo11\', NULL, \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(\'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:22:00', '2019-10-27 09:22:00'),
(14, 'error', 'Swift_RfcComplianceException: Address in mailbox given [liemdo11] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'liemdo11\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(\'liemdo11\', NULL)\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(\'liemdo11\', NULL, \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(\'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:23:04', '2019-10-27 09:23:04'),
(15, 'error', 'Swift_RfcComplianceException: Address in mailbox given [] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(NULL, \'liemdo11\')\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(NULL, \'liemdo11\', \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(NULL, \'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:23:07', '2019-10-27 09:23:07'),
(16, 'error', 'Swift_TransportException: Failed to authenticate on SMTP server with username \"info@alipo.vn\" using 3 possible authenticators. Authenticator LOGIN returned Expected response code 235 but got code \"535\", with message \"535-5.7.8 Username and Password not accepted. Learn more at\r\n535 5.7.8  https://support.google.com/mail/?p=BadCredentials x25sm3933042pfq.73 - gsmtp\r\n\". Authenticator PLAIN returned Expected response code 235 but got code \"535\", with message \"535-5.7.8 Username and Password not accepted. Learn more at\r\n535 5.7.8  https://support.google.com/mail/?p=BadCredentials x25sm3933042pfq.73 - gsmtp\r\n\". Authenticator XOAUTH2 returned Expected response code 250 but got code \"535\", with message \"535-5.7.8 Username and Password not accepted. Learn more at\r\n535 5.7.8  https://support.google.com/mail/?p=BadCredentials x25sm3933042pfq.73 - gsmtp\r\n\". in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/Esmtp/AuthHandler.php:191\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/EsmtpTransport.php(371): Swift_Transport_Esmtp_AuthHandler->afterEhlo(Object(Swift_SmtpTransport))\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php(148): Swift_Transport_EsmtpTransport->doHeloCommand()\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mailer.php(65): Swift_Transport_AbstractSmtpTransport->start()\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(451): Swift_Mailer->send(Object(Swift_Message), Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(115): Illuminate\\Mail\\Mailer->sendSwiftMessage(Object(Swift_Message))\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(115): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#10 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#13 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#53 {main}', NULL, '2019-10-27 09:25:34', '2019-10-27 09:25:34');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(17, 'error', 'Swift_RfcComplianceException: Address in mailbox given [] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(NULL, \'liemdo11\')\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(NULL, \'liemdo11\', \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(NULL, \'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:29:55', '2019-10-27 09:29:55'),
(18, 'error', 'Swift_RfcComplianceException: Address in mailbox given [liemdo11] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'liemdo11\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(\'liemdo11\', NULL)\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(\'liemdo11\', NULL, \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(\'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:30:30', '2019-10-27 09:30:30'),
(19, 'error', 'Swift_RfcComplianceException: Address in mailbox given [liemdo11] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'liemdo11\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(\'liemdo11\', NULL)\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(\'liemdo11\', NULL, \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(\'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:36:28', '2019-10-27 09:36:28'),
(20, 'error', 'Swift_RfcComplianceException: Address in mailbox given [liemdo11] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'liemdo11\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(\'liemdo11\', NULL)\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(\'liemdo11\', NULL, \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(\'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:36:31', '2019-10-27 09:36:31');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(21, 'error', 'Swift_RfcComplianceException: Address in mailbox given [liemdo11] does not comply with RFC 2822, 3.6.2. in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php:355\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(272): Swift_Mime_Headers_MailboxHeader->assertValidAddress(\'liemdo11\')\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(117): Swift_Mime_Headers_MailboxHeader->normalizeMailboxes(Array)\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/Headers/MailboxHeader.php(74): Swift_Mime_Headers_MailboxHeader->setNameAddresses(Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderFactory.php(61): Swift_Mime_Headers_MailboxHeader->setFieldBodyModel(Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleHeaderSet.php(71): Swift_Mime_SimpleHeaderFactory->createMailboxHeader(\'Reply-To\', Array)\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(268): Swift_Mime_SimpleHeaderSet->addMailboxHeader(\'Reply-To\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php(245): Swift_Mime_SimpleMessage->setReplyTo(Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(161): Swift_Mime_SimpleMessage->addReplyTo(\'liemdo11\', NULL)\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Message.php(145): Illuminate\\Mail\\Message->addAddresses(\'liemdo11\', NULL, \'ReplyTo\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(111): Illuminate\\Mail\\Message->replyTo(\'liemdo11\')\n#10 [internal function]: LaminSanneh\\FlexiContact\\components\\ContactForm->LaminSanneh\\FlexiContact\\components\\{closure}(Object(Illuminate\\Mail\\Message))\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(77): call_user_func(Object(Closure), Object(Illuminate\\Mail\\Message))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#13 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(114): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#58 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#59 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#60 {main}', NULL, '2019-10-27 09:36:34', '2019-10-27 09:36:34'),
(22, 'error', 'Swift_TransportException: Failed to authenticate on SMTP server with username \"info@alipo.vn\" using 3 possible authenticators. Authenticator LOGIN returned Expected response code 235 but got code \"535\", with message \"535-5.7.8 Username and Password not accepted. Learn more at\r\n535 5.7.8  https://support.google.com/mail/?p=BadCredentials s7sm8589939pgq.91 - gsmtp\r\n\". Authenticator PLAIN returned Expected response code 235 but got code \"535\", with message \"535-5.7.8 Username and Password not accepted. Learn more at\r\n535 5.7.8  https://support.google.com/mail/?p=BadCredentials s7sm8589939pgq.91 - gsmtp\r\n\". Authenticator XOAUTH2 returned Expected response code 250 but got code \"535\", with message \"535-5.7.8 Username and Password not accepted. Learn more at\r\n535 5.7.8  https://support.google.com/mail/?p=BadCredentials s7sm8589939pgq.91 - gsmtp\r\n\". in /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/Esmtp/AuthHandler.php:191\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/EsmtpTransport.php(371): Swift_Transport_Esmtp_AuthHandler->afterEhlo(Object(Swift_SmtpTransport))\n#1 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php(148): Swift_Transport_EsmtpTransport->doHeloCommand()\n#2 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mailer.php(65): Swift_Transport_AbstractSmtpTransport->start()\n#3 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(451): Swift_Mailer->send(Object(Swift_Message), Array)\n#4 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Mail/Mailer.php(115): Illuminate\\Mail\\Mailer->sendSwiftMessage(Object(Swift_Message))\n#5 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#6 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/laminsanneh/flexicontact/components/ContactForm.php(113): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#7 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#8 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#10 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#11 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#12 /Applications/XAMPP/xamppfiles/htdocs/bbmart/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#13 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#14 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#15 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#16 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#17 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#18 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#19 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /Applications/XAMPP/xamppfiles/htdocs/bbmart/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#37 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#40 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#41 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#42 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#44 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#48 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#49 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#50 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#51 /Applications/XAMPP/xamppfiles/htdocs/bbmart/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#52 /Applications/XAMPP/xamppfiles/htdocs/bbmart/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#53 {main}', NULL, '2019-10-27 09:40:01', '2019-10-27 09:40:01');

-- --------------------------------------------------------

--
-- Table structure for table `system_files`
--

CREATE TABLE `system_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `disk_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_files`
--

INSERT INTO `system_files` (`id`, `disk_name`, `file_name`, `file_size`, `content_type`, `title`, `description`, `field`, `attachment_id`, `attachment_type`, `is_public`, `sort_order`, `created_at`, `updated_at`) VALUES
(50, '5d257e0e0125d223383567.jpg', '7.jpg', 341875, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 50, '2019-07-09 22:56:30', '2019-07-09 22:56:30'),
(51, '5d257ed128ba0461877453.jpg', 'bg.jpg', 557891, 'image/jpeg', NULL, NULL, 'introduction_bg', '1', 'Alipo\\Page\\Models\\Home', 1, 51, '2019-07-09 22:59:45', '2019-07-09 22:59:45'),
(59, '5d25842aa7f61313996764.jpg', '2.jpg', 242484, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 59, '2019-07-09 23:22:34', '2019-07-09 23:22:34'),
(60, '5d25842aee66a086447043.jpg', '1.jpg', 326844, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 60, '2019-07-09 23:22:34', '2019-07-09 23:22:34'),
(61, '5d25842c556bd922293616.jpg', '3.jpg', 257157, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 61, '2019-07-09 23:22:36', '2019-07-09 23:22:36'),
(77, '5d25892904ec8136201594.png', 'SMCT-Logo.png', 11008, 'image/png', NULL, NULL, NULL, NULL, NULL, 1, 77, '2019-07-09 23:43:53', '2019-07-09 23:43:53'),
(78, '5d258931397b6898930978.png', 'SMCT-Footer-Logo.png', 57243, 'image/png', NULL, NULL, NULL, NULL, NULL, 1, 78, '2019-07-09 23:44:01', '2019-07-09 23:44:01'),
(92, '5d25ae1dad91d156498714.jpg', 'photo-2-1560397511124669000399.jpg', 162615, 'image/jpeg', NULL, NULL, 'thumbnail', '1', 'Alipo\\Post\\Models\\Post', 1, 92, '2019-07-10 02:21:33', '2019-07-10 02:22:03'),
(111, '5d25fb93de20d380152182.jpg', 'd22b3aee38f8dca685e9.jpg', 485549, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 111, '2019-07-10 07:52:03', '2019-07-10 07:52:03'),
(112, '5d25fbc2ebdea406231739.jpg', '05Kita_Trucchinh.jpg', 385075, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Location', 1, 112, '2019-07-10 07:52:50', '2019-07-10 07:52:54'),
(113, '5d25fbc446d59722492570.jpg', '852c291b2b0dcf53961c.jpg', 416618, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Location', 1, 113, '2019-07-10 07:52:52', '2019-07-10 07:52:54'),
(114, '5d25fbc670011323252228.jpg', 'd22b3aee38f8dca685e9.jpg', 485549, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Location', 1, 114, '2019-07-10 07:52:54', '2019-07-10 07:53:41'),
(115, '5d25fc93d975d766775778.jpg', '852c291b2b0dcf53961c.jpg', 416618, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Facility', 1, 115, '2019-07-10 07:56:19', '2019-07-10 07:56:43'),
(116, '5d25fc9419b00621608405.jpg', '05Kita_Trucchinh.jpg', 385075, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Facility', 1, 116, '2019-07-10 07:56:20', '2019-07-10 07:56:43'),
(117, '5d25fc965bdf0199898009.jpg', 'd22b3aee38f8dca685e9.jpg', 485549, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Facility', 1, 117, '2019-07-10 07:56:22', '2019-07-10 07:56:43'),
(118, '5d25fce754ae6126512454.jpg', 'd22b3aee38f8dca685e9.jpg', 485549, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\TypeOfHouse', 1, 118, '2019-07-10 07:57:43', '2019-07-10 07:57:45'),
(119, '5d25fce79d824263844859.jpg', '852c291b2b0dcf53961c.jpg', 416618, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\TypeOfHouse', 1, 119, '2019-07-10 07:57:43', '2019-07-10 07:57:45'),
(120, '5d25fd112dad7737347672.jpg', '04Kita_MasterplanNight.jpg', 689216, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\News', 1, 120, '2019-07-10 07:58:25', '2019-07-10 07:58:26'),
(121, '5d25fd3680ebc025992147.jpg', '852c291b2b0dcf53961c.jpg', 416618, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Contact', 1, 121, '2019-07-10 07:59:02', '2019-07-10 07:59:03'),
(123, '5d25fe1926404771141444.jpg', 'map1.jpg', 910805, 'image/jpeg', NULL, NULL, 'postion_image', '1', 'Alipo\\Page\\Models\\Location', 1, 123, '2019-07-10 08:02:49', '2019-07-10 08:02:51'),
(130, '5d2700ff8dc15231526258.jpg', 'Vườn-bonsai_.jpg', 2252526, 'image/jpeg', NULL, NULL, 'images', '4', 'Alipo\\Page\\Models\\FacilityDetail', 1, 130, '2019-07-11 02:27:27', '2019-07-11 02:27:29'),
(131, '5d270110db48c317373879.jpg', 'TT-Hội-nghị.jpg', 2324028, 'image/jpeg', NULL, NULL, 'images', '3', 'Alipo\\Page\\Models\\FacilityDetail', 1, 131, '2019-07-11 02:27:46', '2019-07-11 02:27:49'),
(133, '5d270138ea399983046274.jpg', 'TT-TM-KS.jpg', 703008, 'image/jpeg', NULL, NULL, 'images', '2', 'Alipo\\Page\\Models\\FacilityDetail', 1, 133, '2019-07-11 02:28:24', '2019-07-11 02:28:25'),
(134, '5d27020c4347c872371729.jpg', '22222.jpg', 2299145, 'image/jpeg', NULL, NULL, 'images', '1', 'Alipo\\Page\\Models\\FacilityDetail', 1, 134, '2019-07-11 02:31:57', '2019-07-11 02:31:58'),
(137, '5d27fbf158e27731951748.jpg', '7.jpg', 341875, 'image/jpeg', NULL, NULL, 'picture', '1', 'Alipo\\Project\\Models\\Project', 1, 137, '2019-07-11 20:18:09', '2019-07-11 20:18:12'),
(138, '5d2edcd29d4f0541789597.jpg', 'Stella-1.jpg', 45836, 'image/jpeg', NULL, NULL, 'thumbnail', '4', 'Alipo\\Post\\Models\\Post', 1, 138, '2019-07-17 01:31:14', '2019-07-17 01:33:35'),
(140, '5d2ee0b929304279025407.jpg', 'Stella.jpg', 55544, 'image/jpeg', NULL, NULL, 'thumbnail', '5', 'Alipo\\Post\\Models\\Post', 1, 140, '2019-07-17 01:47:53', '2019-07-17 01:48:48'),
(141, '5d2ee21c11b3f397676136.jpg', 'stella-mega-city-can-tho-phon-vinh-cuoc-song-tay-do.jpg', 192851, 'image/jpeg', NULL, NULL, 'thumbnail', '6', 'Alipo\\Post\\Models\\Post', 1, 141, '2019-07-17 01:53:48', '2019-07-17 01:54:36'),
(142, '5d2ee33c0abee060790004.jpg', 'stella-mega-city-can-tho-phon-vinh-cuoc-song-tay-do.jpg', 192851, 'image/jpeg', NULL, NULL, 'thumbnail', '7', 'Alipo\\Post\\Models\\Post', 1, 142, '2019-07-17 01:58:36', '2019-07-17 01:58:44'),
(143, '5d2ee43f9e09c979157473.jpg', 'megacity3_oeso.jpg', 142021, 'image/jpeg', NULL, NULL, 'thumbnail', '8', 'Alipo\\Post\\Models\\Post', 1, 143, '2019-07-17 02:02:55', '2019-07-17 02:02:56'),
(144, '5d2ee4c21767b619566222.jpg', 'stella-mega-city-can-tho-phon-vinh-cuoc-song-tay-do-1.jpg', 190191, 'image/jpeg', NULL, NULL, 'thumbnail', '9', 'Alipo\\Post\\Models\\Post', 1, 144, '2019-07-17 02:05:06', '2019-07-17 02:05:07'),
(145, '5d3d2d73677e7763405685.jpg', 'Massage_02.jpg', 176848, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 145, '2019-07-27 22:06:59', '2019-07-27 22:06:59'),
(150, '5d3d3017ec30d966733669.jpg', 'facilities-slider-01.jpg', 519985, 'image/jpeg', NULL, NULL, 'process_image', '1', 'Alipo\\Page\\Models\\Process', 1, 150, '2019-07-27 22:18:15', '2019-07-27 22:19:02'),
(151, '5d3d30180aa69289280741.jpg', 'facilities-slider-02.jpg', 505379, 'image/jpeg', NULL, NULL, 'process_image', '1', 'Alipo\\Page\\Models\\Process', 1, 151, '2019-07-27 22:18:16', '2019-07-27 22:19:02'),
(152, '5d3d30186dc14492035843.jpg', 'facilities-slider-03.jpg', 519985, 'image/jpeg', NULL, NULL, 'process_image', '1', 'Alipo\\Page\\Models\\Process', 1, 152, '2019-07-27 22:18:16', '2019-07-27 22:19:02'),
(153, '5d3d30188eca7034476900.jpg', 'facilities-slider-04.jpg', 309219, 'image/jpeg', NULL, NULL, 'process_image', '1', 'Alipo\\Page\\Models\\Process', 1, 153, '2019-07-27 22:18:16', '2019-07-27 22:19:02'),
(159, '5d47c511adbcf078279971.jpg', 'Untitled-1-2.jpg', 1725126, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Process', 1, 159, '2019-08-04 22:56:33', '2019-08-04 22:56:38'),
(160, '5d4de57a84986327268141.jpg', '5d25fa831ad47647141332.jpg', 385075, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 160, '2019-08-09 14:28:26', '2019-08-09 14:28:26'),
(161, '5d4de57a8d9fb356711160.jpg', '5d25fa877b04e593051331 (1).jpg', 485549, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 161, '2019-08-09 14:28:26', '2019-08-09 14:28:26'),
(162, '5d4de57d7c844287031660.jpg', '5d25fa8602796032361536.jpg', 416618, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 162, '2019-08-09 14:28:29', '2019-08-09 14:28:29'),
(163, '5d4de57d7dd41450931039.jpg', '5d25fa877b04e593051331.jpg', 485549, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 163, '2019-08-09 14:28:29', '2019-08-09 14:28:29'),
(174, '5d4df32b7d6ac208181142.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_1', '1', 'Alipo\\Page\\Models\\Home', 1, 174, '2019-08-09 15:26:51', '2019-08-09 15:27:26'),
(175, '5d4df3381bf3d994116088.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_2', '1', 'Alipo\\Page\\Models\\Home', 1, 175, '2019-08-09 15:27:04', '2019-08-09 15:27:26'),
(176, '5d4df33c8e8d7006212761.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_3', '1', 'Alipo\\Page\\Models\\Home', 1, 176, '2019-08-09 15:27:08', '2019-08-09 15:27:26'),
(177, '5d4df33f2ba5e001770649.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_4', '1', 'Alipo\\Page\\Models\\Home', 1, 177, '2019-08-09 15:27:11', '2019-08-09 15:27:26'),
(178, '5d4df342d7118157418731.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_5', '1', 'Alipo\\Page\\Models\\Home', 1, 178, '2019-08-09 15:27:14', '2019-08-09 15:27:26'),
(179, '5d4df345acf5b847107676.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_6', '1', 'Alipo\\Page\\Models\\Home', 1, 179, '2019-08-09 15:27:17', '2019-08-09 15:27:26'),
(180, '5d4df349933af562724563.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_7', '1', 'Alipo\\Page\\Models\\Home', 1, 180, '2019-08-09 15:27:21', '2019-08-09 15:27:26'),
(181, '5d4df34c9c158892867255.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_8', '1', 'Alipo\\Page\\Models\\Home', 1, 181, '2019-08-09 15:27:24', '2019-08-09 15:27:26'),
(182, '5d4efd0421e9f918391439.jpg', '8e87bb2caa414e1f1750.jpg', 1541828, 'image/jpeg', NULL, NULL, 'images', '5', 'Alipo\\Page\\Models\\FacilityDetail', 1, 182, '2019-08-10 10:21:10', '2019-08-10 10:21:13'),
(183, '5d4efd343c47c338541903.jpg', '09Kita_TruongQuocTe.jpg', 10610523, 'image/jpeg', NULL, NULL, 'images', '6', 'Alipo\\Page\\Models\\FacilityDetail', 1, 183, '2019-08-10 10:22:01', '2019-08-10 10:22:13'),
(192, '5d5c9398da3a0530914882.png', 'logo.png', 11008, 'image/png', NULL, NULL, 'logo', '1', 'Alipo\\GeneralOption\\Models\\General', 1, 192, '2019-08-20 17:43:04', '2019-08-20 17:43:05'),
(193, '5d60b3230b324377673372.jpg', 'bg-footer.jpg', 88673, 'image/jpeg', NULL, NULL, 'background', '1', 'Alipo\\GeneralOption\\Models\\General', 1, 193, '2019-08-23 20:46:43', '2019-08-23 20:48:13'),
(194, '5d60b32f60d4a439282839.png', 'logo-footer.png', 15414, 'image/png', NULL, NULL, 'footer_logo', '1', 'Alipo\\GeneralOption\\Models\\General', 1, 194, '2019-08-23 20:46:55', '2019-08-23 20:48:13'),
(196, '5d61890d0b192728076040.jpg', 'bg-contact.jpg', 134023, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Gallery', 1, 196, '2019-08-24 11:59:25', '2019-08-24 12:02:05'),
(199, '5d62300420a51539764612.jpg', '5d25fb91c553a010940191.jpg', 385075, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Introduction', 1, 199, '2019-08-24 23:51:48', '2019-08-24 23:51:49'),
(206, '5d62502597c6b484836034.jpg', 'bg-tong-quan.jpg', 244017, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 206, '2019-08-25 02:08:53', '2019-08-25 02:08:53'),
(208, '5d6252f8f0f7c141851123.jpg', 'facilities-slider-04.jpg', 309219, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\ChuDauTu', 1, 208, '2019-08-25 02:20:56', '2019-08-25 02:20:58'),
(216, '5d6d4aad87ee5220719837.png', 'logo-header.png', 31710, 'image/png', NULL, NULL, 'logo', '3', 'Backend\\Models\\BrandSetting', 1, 216, '2019-09-02 10:00:29', '2019-09-02 10:00:56'),
(217, '5d6d4ab55d650738477558.png', 'favicon.png', 7390, 'image/png', NULL, NULL, 'favicon', '3', 'Backend\\Models\\BrandSetting', 1, 217, '2019-09-02 10:00:37', '2019-09-02 10:00:56'),
(218, '5d7287d646351339124236.jpg', '5d4f0040b1e9c147286098.jpg', 623450, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 218, '2019-09-06 09:22:47', '2019-09-06 09:22:47'),
(219, '5d7287d9a2fed400409722.jpg', '5d4f004365dd6862027485.jpg', 672981, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 219, '2019-09-06 09:22:49', '2019-09-06 09:22:50'),
(220, '5d7287da52a67334851487.jpg', '5d4f004375eb4495804463.jpg', 498027, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 220, '2019-09-06 09:22:50', '2019-09-06 09:22:50'),
(225, '5d728a69197bc578753829.jpg', '5d6293e309eca528701155.jpg', 482256, 'image/jpeg', NULL, NULL, 'image', '4', 'Alipo\\Page\\Models\\FacilityDetail', 1, 225, '2019-09-06 09:33:45', '2019-09-06 09:33:47'),
(226, '5d728a78b6371652198696.jpg', 'trungtamhoinghi.jpg', 503976, 'image/jpeg', NULL, NULL, 'image', '3', 'Alipo\\Page\\Models\\FacilityDetail', 1, 226, '2019-09-06 09:34:00', '2019-09-06 09:34:09'),
(227, '5d728a91089ea448269284.jpg', 'quntruong.jpg', 435031, 'image/jpeg', NULL, NULL, 'image', '1', 'Alipo\\Page\\Models\\FacilityDetail', 1, 227, '2019-09-06 09:34:25', '2019-09-06 09:34:30'),
(228, '5d728aa31af80995056871.jpg', 'hethongtruonghoc.jpg', 546404, 'image/jpeg', NULL, NULL, 'image', '6', 'Alipo\\Page\\Models\\FacilityDetail', 1, 228, '2019-09-06 09:34:43', '2019-09-06 09:34:44'),
(230, '5d728ad2c1c35844761192.jpg', 'quntruong.jpg', 435031, 'image/jpeg', NULL, NULL, 'image', '2', 'Alipo\\Page\\Models\\FacilityDetail', 1, 230, '2019-09-06 09:35:30', '2019-09-06 09:35:55'),
(231, '5d728af726fb6052486837.jpg', 'congvien.jpg', 482256, 'image/jpeg', NULL, NULL, 'image', '5', 'Alipo\\Page\\Models\\FacilityDetail', 1, 231, '2019-09-06 09:36:07', '2019-09-06 09:36:08'),
(232, '5d728ce8a4a6c329460421.jpg', '5d27fbb2c4a57943777281.jpg', 690718, 'image/jpeg', NULL, NULL, 'layout', '1', 'Alipo\\Project\\Models\\Project', 1, 232, '2019-09-06 09:44:24', '2019-09-06 09:44:29'),
(233, '5d728cec27512044538188.jpg', '5d27fbb2c4a57943777281.jpg', 690718, 'image/jpeg', NULL, NULL, 'spherical_Face', '1', 'Alipo\\Project\\Models\\Project', 1, 233, '2019-09-06 09:44:28', '2019-09-06 09:44:29'),
(234, '5d91ab906892d292508664.jpg', '178651569593014-1569641443.jpg', 66721, 'image/jpeg', NULL, NULL, 'thumbnail', '10', 'Alipo\\Post\\Models\\Post', 1, 234, '2019-09-30 00:15:28', '2019-09-30 00:15:34'),
(235, '5d91acf97500e110986487.png', 'Screen Shot 2019-09-30 at 2.20.57 PM.png', 227888, 'image/png', NULL, NULL, 'thumbnail', '11', 'Alipo\\Post\\Models\\Post', 1, 235, '2019-09-30 00:21:29', '2019-09-30 00:21:31'),
(236, '5d91b45c3833b194577045.jpg', 'Du-an-moi-tai-Can-Tho-giup-nguoi-dan-cham-tay-den-uoc-mo-cuoc-song-an-lanh--thinh-vuong-can-tho-1-1569567033-8-width660height371.jpg', 152564, 'image/jpeg', NULL, NULL, 'thumbnail', '12', 'Alipo\\Post\\Models\\Post', 1, 236, '2019-09-30 00:53:00', '2019-09-30 00:53:03'),
(238, '5d91b5a72c39e845862854.jpg', '2-1569659750.jpg', 152555, 'image/jpeg', NULL, NULL, 'thumbnail', '13', 'Alipo\\Post\\Models\\Post', 1, 238, '2019-09-30 00:58:31', '2019-09-30 00:58:34'),
(239, '5d91b700986f4424665131.jpg', 'photo-2-15696772333881180126621.jpg', 133941, 'image/jpeg', NULL, NULL, 'thumbnail', '14', 'Alipo\\Post\\Models\\Post', 1, 239, '2019-09-30 01:04:16', '2019-09-30 01:04:19'),
(240, '5d91b8cd16744908093752.jpg', 'photo-2-15696772333881180126621.jpg', 138988, 'image/jpeg', NULL, NULL, 'thumbnail', '15', 'Alipo\\Post\\Models\\Post', 1, 240, '2019-09-30 01:11:57', '2019-09-30 01:11:59'),
(241, '5d91b984b7c81641791438.jpg', '2-1569659750.jpg', 300110, 'image/jpeg', NULL, NULL, 'thumbnail', '16', 'Alipo\\Post\\Models\\Post', 1, 241, '2019-09-30 01:15:00', '2019-09-30 01:15:03'),
(242, '5db44f01cccf8167921958.jpg', 'bbmart-bg.jpg', 380186, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Home', 1, 242, '2019-10-26 06:49:53', '2019-10-26 06:49:58');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_layouts`
--

CREATE TABLE `system_mail_layouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `content_css` text COLLATE utf8mb4_unicode_ci,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `options` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_layouts`
--

INSERT INTO `system_mail_layouts` (`id`, `name`, `code`, `content_html`, `content_text`, `content_css`, `is_locked`, `options`, `created_at`, `updated_at`) VALUES
(1, 'Default layout', 'default', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-default\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n\n        <!-- Header -->\n        {% partial \'header\' body %}\n            {{ subject|raw }}\n        {% endpartial %}\n\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n\n        <!-- Footer -->\n        {% partial \'footer\' body %}\n            &copy; {{ \"now\"|date(\"Y\") }} {{ appName }}. All rights reserved.\n        {% endpartial %}\n\n    </table>\n\n</body>\n</html>', '{{ content|raw }}', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, NULL, '2019-07-07 10:08:09', '2019-07-07 10:08:09'),
(2, 'System layout', 'system', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-system\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n\n                                        <!-- Subcopy -->\n                                        {% partial \'subcopy\' body %}\n                                            **This is an automatic message. Please do not reply to it.**\n                                        {% endpartial %}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n    </table>\n\n</body>\n</html>', '{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, NULL, '2019-07-07 10:08:09', '2019-07-07 10:08:09');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_partials`
--

CREATE TABLE `system_mail_partials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_partials`
--

INSERT INTO `system_mail_partials` (`id`, `name`, `code`, `content_html`, `content_text`, `is_custom`, `created_at`, `updated_at`) VALUES
(1, 'Header', 'header', '<tr>\n    <td class=\"header\">\n        {% if url %}\n            <a href=\"{{ url }}\">\n                {{ body }}\n            </a>\n        {% else %}\n            <span>\n                {{ body }}\n            </span>\n        {% endif %}\n    </td>\n</tr>', '*** {{ body|trim }} <{{ url }}>', 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20'),
(2, 'Footer', 'footer', '<tr>\n    <td>\n        <table class=\"footer\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n            <tr>\n                <td class=\"content-cell\" align=\"center\">\n                    {{ body|md_safe }}\n                </td>\n            </tr>\n        </table>\n    </td>\n</tr>', '-------------------\n{{ body|trim }}', 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20'),
(3, 'Button', 'button', '<table class=\"action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td align=\"center\">\n            <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                <tr>\n                    <td align=\"center\">\n                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                            <tr>\n                                <td>\n                                    <a href=\"{{ url }}\" class=\"button button-{{ type ?: \'primary\' }}\" target=\"_blank\">\n                                        {{ body }}\n                                    </a>\n                                </td>\n                            </tr>\n                        </table>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>', '{{ body|trim }} <{{ url }}>', 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20'),
(4, 'Panel', 'panel', '<table class=\"panel\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td class=\"panel-content\">\n            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                <tr>\n                    <td class=\"panel-item\">\n                        {{ body|md_safe }}\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>', '{{ body|trim }}', 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20'),
(5, 'Table', 'table', '<div class=\"table\">\n    {{ body|md_safe }}\n</div>', '{{ body|trim }}', 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20'),
(6, 'Subcopy', 'subcopy', '<table class=\"subcopy\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td>\n            {{ body|md_safe }}\n        </td>\n    </tr>\n</table>', '-----\n{{ body|trim }}', 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20'),
(7, 'Promotion', 'promotion', '<table class=\"promotion\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td align=\"center\">\n            {{ body|md_safe }}\n        </td>\n    </tr>\n</table>', '{{ body|trim }}', 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_templates`
--

CREATE TABLE `system_mail_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_templates`
--

INSERT INTO `system_mail_templates` (`id`, `code`, `subject`, `description`, `content_html`, `content_text`, `layout_id`, `is_custom`, `created_at`, `updated_at`) VALUES
(1, 'laminsanneh.flexicontact::emails.message', 'http://noithatthongminh.bbsmartdecor.com', 'Liên Hệ Tham Quan Dự Án', '<h2>Đăng Ký Tham Quan Dự Án</h2>\r\n<p>Name: {{name}}</p>\r\n<p>Phone: {{phone}}</p>', '', 1, 1, '2019-07-23 01:31:20', '2019-10-27 09:35:32'),
(2, 'backend::mail.invite', NULL, 'Invite new admin to the site', NULL, NULL, 2, 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20'),
(3, 'backend::mail.restore', NULL, 'Reset an admin password', NULL, NULL, 2, 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20');

-- --------------------------------------------------------

--
-- Table structure for table `system_parameters`
--

CREATE TABLE `system_parameters` (
  `id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_parameters`
--

INSERT INTO `system_parameters` (`id`, `namespace`, `group`, `item`, `value`) VALUES
(1, 'system', 'update', 'count', '0'),
(2, 'cms', 'theme', 'active', '\"cantho\"'),
(3, 'system', 'update', 'retry', '1572236620'),
(4, 'system', 'core', 'build', '455');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_history`
--

CREATE TABLE `system_plugin_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_history`
--

INSERT INTO `system_plugin_history` (`id`, `code`, `type`, `version`, `detail`, `created_at`) VALUES
(1, 'BenFreke.MenuManager', 'script', '1.0.1', 'create_menus_table.php', '2019-07-07 10:08:08'),
(2, 'BenFreke.MenuManager', 'comment', '1.0.1', 'First version of MenuManager', '2019-07-07 10:08:08'),
(3, 'BenFreke.MenuManager', 'comment', '1.0.2', 'Added active state to menu; Added ability to select active menu item; Added controllable depth to component', '2019-07-07 10:08:08'),
(4, 'BenFreke.MenuManager', 'comment', '1.0.3', 'Made it possible for menu items to not link anywhere; Put a check in so the active node must be a child of the parentNode', '2019-07-07 10:08:08'),
(5, 'BenFreke.MenuManager', 'comment', '1.0.4', 'Fixed bug where re-ordering stopped working', '2019-07-07 10:08:08'),
(6, 'BenFreke.MenuManager', 'comment', '1.0.5', 'Moved link creation code into the model in preparation for external links; Brought list item class creation into the model; Fixed typo with default menu class', '2019-07-07 10:08:08'),
(7, 'BenFreke.MenuManager', 'comment', '1.0.6', 'Removed NestedSetModel, thanks @daftspunk', '2019-07-07 10:08:08'),
(8, 'BenFreke.MenuManager', 'script', '1.1.0', 'add_is_external_field.php', '2019-07-07 10:08:08'),
(9, 'BenFreke.MenuManager', 'script', '1.1.0', 'add_link_target_field.php', '2019-07-07 10:08:08'),
(10, 'BenFreke.MenuManager', 'comment', '1.1.0', 'Added ability to link to external sites. Thanks @adisos', '2019-07-07 10:08:08'),
(11, 'BenFreke.MenuManager', 'script', '1.1.1', 'add_enabled_parameters_query_fields_1.php', '2019-07-07 10:08:08'),
(12, 'BenFreke.MenuManager', 'script', '1.1.1', 'add_enabled_parameters_query_fields_2.php', '2019-07-07 10:08:08'),
(13, 'BenFreke.MenuManager', 'script', '1.1.1', 'add_enabled_parameters_query_fields_3.php', '2019-07-07 10:08:08'),
(14, 'BenFreke.MenuManager', 'comment', '1.1.1', 'Added ability to enable/disable individual menu links; Added ability for url parameters & query string; Fixed issue of \"getLinkHref()\" pulling through full page url with parameters rather than the ACTUAL page url', '2019-07-07 10:08:08'),
(15, 'BenFreke.MenuManager', 'comment', '1.1.2', 'Reformatted code for better maintainability and better practises', '2019-07-07 10:08:08'),
(16, 'BenFreke.MenuManager', 'comment', '1.1.3', 'Fixed bug that prevented multiple components displaying', '2019-07-07 10:08:08'),
(17, 'BenFreke.MenuManager', 'comment', '1.2.0', 'Fixed validation and fields bug; Added list classes', '2019-07-07 10:08:08'),
(18, 'BenFreke.MenuManager', 'comment', '1.3.0', 'Added translation ability thanks to @alxy', '2019-07-07 10:08:08'),
(19, 'BenFreke.MenuManager', 'comment', '1.3.1', 'JSON validation of parameters added; Correct active menu now being set based on parameters; PR sent by @whsol, thanks!', '2019-07-07 10:08:08'),
(20, 'BenFreke.MenuManager', 'comment', '1.3.2', 'Fix for param check that is breaking active node lookup. Thanks @alxy', '2019-07-07 10:08:08'),
(21, 'BenFreke.MenuManager', 'comment', '1.3.3', 'Fix for JSON comment having single quotes. Thanks @adisos', '2019-07-07 10:08:08'),
(22, 'BenFreke.MenuManager', 'comment', '1.3.4', 'Fix for JSON validation not firing', '2019-07-07 10:08:08'),
(23, 'BenFreke.MenuManager', 'script', '1.4.0', 'fix_menu_table.php', '2019-07-07 10:08:08'),
(24, 'BenFreke.MenuManager', 'comment', '1.4.0', 'Fix for POST operations. PR by @matissjanis, TR translations (@mahony0) and permission registration (@nnmer)', '2019-07-07 10:08:08'),
(25, 'BenFreke.MenuManager', 'comment', '1.4.1', 'Fixed bug caused by deleting needed method of Menu class. Thanks @MatissJA', '2019-07-07 10:08:08'),
(26, 'BenFreke.MenuManager', 'comment', '1.4.2', 'Fixed bug with URLs not saving correctly', '2019-07-07 10:08:08'),
(27, 'BenFreke.MenuManager', 'comment', '1.4.3', 'Fixed bug where getBaseFileName method was moved to a different object', '2019-07-07 10:08:08'),
(28, 'BenFreke.MenuManager', 'comment', '1.4.4', 'Fixed bug with incorrect labels. Thanks @ribsousa', '2019-07-07 10:08:08'),
(29, 'BenFreke.MenuManager', 'comment', '1.4.5', 'Fixed bug where getBaseFileName method was moved to a different object', '2019-07-07 10:08:08'),
(30, 'BenFreke.MenuManager', 'comment', '1.4.6', 'Merged PRs that fix bug with plugin not working with stable release', '2019-07-07 10:08:08'),
(31, 'BenFreke.MenuManager', 'comment', '1.4.7', 'Merged PR to fix syntax errors with fresh install of 1.4.6. Thanks @devlifeX', '2019-07-07 10:08:08'),
(32, 'BenFreke.MenuManager', 'comment', '1.4.8', 'Merged PR to fix re-order errors. Thanks @CptMeatball', '2019-07-07 10:08:08'),
(33, 'BenFreke.MenuManager', 'comment', '1.5.0', 'Fixed bugs preventing postgres and sqlite support', '2019-07-07 10:08:08'),
(34, 'BenFreke.MenuManager', 'comment', '1.5.1', 'Added homepage to plugin information. Thanks @gergo85', '2019-07-07 10:08:08'),
(35, 'BenFreke.MenuManager', 'comment', '1.5.2', 'Added French translation. Thanks @Glitchbone', '2019-07-07 10:08:08'),
(36, 'BenFreke.MenuManager', 'comment', '1.5.2', 'Added code of conduct', '2019-07-07 10:08:08'),
(37, 'BenFreke.MenuManager', 'comment', '1.5.3', 'Added ability to delete menus. Thanks @osmanzeki', '2019-07-07 10:08:08'),
(38, 'October.Demo', 'comment', '1.0.1', 'First version of Demo', '2019-07-07 10:08:08'),
(39, 'Alipo.Page', 'script', '1.0.2', 'create_contacts_table.php', '2019-07-07 10:08:08'),
(40, 'Alipo.Page', 'script', '1.0.2', 'create_facilities_table.php', '2019-07-07 10:08:08'),
(41, 'Alipo.Page', 'script', '1.0.2', 'create_homes_table.php', '2019-07-07 10:08:08'),
(42, 'Alipo.Page', 'script', '1.0.2', 'create_introductions_table.php', '2019-07-07 10:08:08'),
(43, 'Alipo.Page', 'script', '1.0.2', 'create_locations_table.php', '2019-07-07 10:08:08'),
(44, 'Alipo.Page', 'script', '1.0.2', 'create_news_table.php', '2019-07-07 10:08:08'),
(45, 'Alipo.Page', 'script', '1.0.2', 'create_type_of_houses_table.php', '2019-07-07 10:08:08'),
(46, 'Alipo.Post', 'script', '1.0.1', 'create_categories_table.php', '2019-07-07 10:08:08'),
(47, 'Alipo.Post', 'script', '1.0.1', 'create_category_posts_table.php', '2019-07-07 10:08:09'),
(48, 'Alipo.Post', 'script', '1.0.1', 'create_posts_table.php', '2019-07-07 10:08:09'),
(49, 'Alipo.GeneralOption', 'script', '1.0.1', 'create_generals_table.php', '2019-07-07 10:08:09'),
(50, 'Alipo.Project', 'script', '1.0.2', 'create_projects_table.php', '2019-07-07 10:08:09'),
(51, 'LaminSanneh.FlexiContact', 'comment', '1.0.0', 'First Version with basic functionality', '2019-07-07 10:08:09'),
(52, 'LaminSanneh.FlexiContact', 'comment', '1.0.1', 'Fixed email subject to send actual subject set in backend', '2019-07-07 10:08:09'),
(53, 'LaminSanneh.FlexiContact', 'comment', '1.0.2', 'Added validation to contact form fields', '2019-07-07 10:08:09'),
(54, 'LaminSanneh.FlexiContact', 'comment', '1.0.3', 'Changed body input field type from text to textarea', '2019-07-07 10:08:09'),
(55, 'LaminSanneh.FlexiContact', 'comment', '1.0.4', 'Updated default component markup to use more appropriate looking twitter bootstrap classes', '2019-07-07 10:08:09'),
(56, 'LaminSanneh.FlexiContact', 'comment', '1.0.5', 'Corrected spelling for Marketing on the backend settings', '2019-07-07 10:08:09'),
(57, 'LaminSanneh.FlexiContact', 'comment', '1.0.6', 'Added ability to include bootstrap or not on component config', '2019-07-07 10:08:09'),
(58, 'LaminSanneh.FlexiContact', 'comment', '1.0.6', 'Add proper validation message outputting', '2019-07-07 10:08:09'),
(59, 'LaminSanneh.FlexiContact', 'comment', '1.0.6', 'Added option to include or exclude main script file', '2019-07-07 10:08:09'),
(60, 'LaminSanneh.FlexiContact', 'comment', '1.0.7', 'Updated contact component default markup file', '2019-07-07 10:08:09'),
(61, 'LaminSanneh.FlexiContact', 'comment', '1.0.7', 'Updated readme file', '2019-07-07 10:08:09'),
(62, 'LaminSanneh.FlexiContact', 'comment', '1.1.0', 'Mail template is now registered properly', '2019-07-07 10:08:09'),
(63, 'LaminSanneh.FlexiContact', 'comment', '1.2.0', 'Add Proper validation that can be localized', '2019-07-07 10:08:09'),
(64, 'LaminSanneh.FlexiContact', 'comment', '1.2.1', 'Added permissions to the settings page. PR by @matissjanis', '2019-07-07 10:08:09'),
(65, 'LaminSanneh.FlexiContact', 'comment', '1.2.2', 'Added polish language features', '2019-07-07 10:08:09'),
(66, 'LaminSanneh.FlexiContact', 'comment', '1.2.3', 'Modified mail templatedefault text', '2019-07-07 10:08:09'),
(67, 'LaminSanneh.FlexiContact', 'comment', '1.3.0', '!!! Added captcha feature, which requires valid google captcha credentials to work', '2019-07-07 10:08:09'),
(68, 'LaminSanneh.FlexiContact', 'comment', '1.3.1', 'Set replyTo instead of from-header when sending', '2019-07-07 10:08:09'),
(69, 'LaminSanneh.FlexiContact', 'comment', '1.3.2', 'Added german translation language file', '2019-07-07 10:08:09'),
(70, 'LaminSanneh.FlexiContact', 'comment', '1.3.3', 'Added option to allow user to enable or disable captcha in contact form', '2019-07-07 10:08:09'),
(71, 'LaminSanneh.FlexiContact', 'comment', '1.3.4', 'Made sure captcha enabling or disabling doesnt produce bug', '2019-07-07 10:08:09'),
(72, 'AnandPatel.WysiwygEditors', 'comment', '1.0.1', 'First version of Wysiwyg Editors.', '2019-07-07 10:08:09'),
(73, 'AnandPatel.WysiwygEditors', 'comment', '1.0.2', 'Automatic Injection to CMS & other code editors and October CMS`s Rich Editor added.', '2019-07-07 10:08:09'),
(74, 'AnandPatel.WysiwygEditors', 'comment', '1.0.3', 'Automatic Injection to RainLab Static Pages & other plugin`s option is appear only if installed.', '2019-07-07 10:08:09'),
(75, 'AnandPatel.WysiwygEditors', 'comment', '1.0.4', 'New Froala editor added (on request from emzero439), Height & width property added for editor, setting moved to My Setting tab and minor changes in settings.', '2019-07-07 10:08:09'),
(76, 'AnandPatel.WysiwygEditors', 'comment', '1.0.5', 'Automatic Injection to Radiantweb`s Problog and ProEvents (option available in settings-content).', '2019-07-07 10:08:09'),
(77, 'AnandPatel.WysiwygEditors', 'comment', '1.0.6', 'CKEditor updated and bug fixes.', '2019-07-07 10:08:09'),
(78, 'AnandPatel.WysiwygEditors', 'comment', '1.0.7', 'Integrated elFinder (file browser) with TinyMCE & CKEditor, Image upload/delete for Froala Editor.', '2019-07-07 10:08:09'),
(79, 'AnandPatel.WysiwygEditors', 'comment', '1.0.8', 'Added security to File Browser`s route (Authenticate users can only access File Browser).', '2019-07-07 10:08:09'),
(80, 'AnandPatel.WysiwygEditors', 'comment', '1.0.9', 'Updated CKEditor, Froala and TinyMCE.', '2019-07-07 10:08:09'),
(81, 'AnandPatel.WysiwygEditors', 'comment', '1.1.0', 'Support multilanguage, update elFinder and cleanup code.', '2019-07-07 10:08:09'),
(82, 'AnandPatel.WysiwygEditors', 'comment', '1.1.1', 'Added Turkish language.', '2019-07-07 10:08:09'),
(83, 'AnandPatel.WysiwygEditors', 'comment', '1.1.2', 'Added Hungarian language.', '2019-07-07 10:08:09'),
(84, 'AnandPatel.WysiwygEditors', 'comment', '1.1.3', 'Fixed issue related to RC (Elfinder fix remaining).', '2019-07-07 10:08:09'),
(85, 'AnandPatel.WysiwygEditors', 'comment', '1.1.4', 'Fixed Elfinder issue.', '2019-07-07 10:08:09'),
(86, 'AnandPatel.WysiwygEditors', 'comment', '1.1.5', 'Updated CKEditor, Froala and TinyMCE.', '2019-07-07 10:08:09'),
(87, 'AnandPatel.WysiwygEditors', 'comment', '1.1.6', 'Changed destination folder.', '2019-07-07 10:08:09'),
(88, 'AnandPatel.WysiwygEditors', 'comment', '1.1.7', 'Added Czech language.', '2019-07-07 10:08:09'),
(89, 'AnandPatel.WysiwygEditors', 'comment', '1.1.8', 'Added Russian language.', '2019-07-07 10:08:09'),
(90, 'AnandPatel.WysiwygEditors', 'comment', '1.1.9', 'Fix hook and other issues (thanks to Vojta Svoboda).', '2019-07-07 10:08:09'),
(91, 'AnandPatel.WysiwygEditors', 'comment', '1.2.0', 'Put settings inside CMS sidemenu (thanks to Amanda Tresbach).', '2019-07-07 10:08:09'),
(92, 'AnandPatel.WysiwygEditors', 'comment', '1.2.1', 'Remove el-finder (Which fix issue of composer), added OC media manager support for image in TinyMCE & CkEditor, TinyMCE version updated, Fix Image upload for froala editor', '2019-07-07 10:08:09'),
(93, 'AnandPatel.WysiwygEditors', 'comment', '1.2.2', 'Add multilingual support, Add new languages, Update the Hungarian language, Add the missing English language files (Special thanks to Szabó Gergő)', '2019-07-07 10:08:09'),
(94, 'AnandPatel.WysiwygEditors', 'comment', '1.2.3', 'Added toolbar customization option (Special thanks to Szabó Gergő).', '2019-07-07 10:08:09'),
(95, 'AnandPatel.WysiwygEditors', 'comment', '1.2.4', 'Added support for Content Plus Plugin & News and Newsletter plugin (thanks to Szabó Gergő)', '2019-07-07 10:08:09'),
(96, 'AnandPatel.WysiwygEditors', 'comment', '1.2.5', 'Minor improvements and bugfixes.', '2019-07-07 10:08:09'),
(97, 'AnandPatel.WysiwygEditors', 'comment', '1.2.6', 'Updated the CKEditor and TinyMCE editors.', '2019-07-07 10:08:09'),
(98, 'AnandPatel.WysiwygEditors', 'comment', '1.2.7', 'Show locale switcher when using multilocale editor.', '2019-07-07 10:08:09'),
(99, 'AnandPatel.WysiwygEditors', 'comment', '1.2.8', 'Added French language', '2019-07-07 10:08:09'),
(100, 'AnandPatel.WysiwygEditors', 'comment', '1.2.9', 'Added permission for preferences', '2019-07-07 10:08:09'),
(101, 'RainLab.Translate', 'script', '1.0.1', 'create_messages_table.php', '2019-07-07 10:08:09'),
(102, 'RainLab.Translate', 'script', '1.0.1', 'create_attributes_table.php', '2019-07-07 10:08:09'),
(103, 'RainLab.Translate', 'script', '1.0.1', 'create_locales_table.php', '2019-07-07 10:08:09'),
(104, 'RainLab.Translate', 'comment', '1.0.1', 'First version of Translate', '2019-07-07 10:08:09'),
(105, 'RainLab.Translate', 'comment', '1.0.2', 'Languages and Messages can now be deleted.', '2019-07-07 10:08:09'),
(106, 'RainLab.Translate', 'comment', '1.0.3', 'Minor updates for latest October release.', '2019-07-07 10:08:09'),
(107, 'RainLab.Translate', 'comment', '1.0.4', 'Locale cache will clear when updating a language.', '2019-07-07 10:08:09'),
(108, 'RainLab.Translate', 'comment', '1.0.5', 'Add Spanish language and fix plugin config.', '2019-07-07 10:08:09'),
(109, 'RainLab.Translate', 'comment', '1.0.6', 'Minor improvements to the code.', '2019-07-07 10:08:09'),
(110, 'RainLab.Translate', 'comment', '1.0.7', 'Fixes major bug where translations are skipped entirely!', '2019-07-07 10:08:09'),
(111, 'RainLab.Translate', 'comment', '1.0.8', 'Minor bug fixes.', '2019-07-07 10:08:09'),
(112, 'RainLab.Translate', 'comment', '1.0.9', 'Fixes an issue where newly created models lose their translated values.', '2019-07-07 10:08:09'),
(113, 'RainLab.Translate', 'comment', '1.0.10', 'Minor fix for latest build.', '2019-07-07 10:08:09'),
(114, 'RainLab.Translate', 'comment', '1.0.11', 'Fix multilingual rich editor when used in stretch mode.', '2019-07-07 10:08:09'),
(115, 'RainLab.Translate', 'comment', '1.1.0', 'Introduce compatibility with RainLab.Pages plugin.', '2019-07-07 10:08:09'),
(116, 'RainLab.Translate', 'comment', '1.1.1', 'Minor UI fix to the language picker.', '2019-07-07 10:08:09'),
(117, 'RainLab.Translate', 'comment', '1.1.2', 'Add support for translating Static Content files.', '2019-07-07 10:08:09'),
(118, 'RainLab.Translate', 'comment', '1.1.3', 'Improved support for the multilingual rich editor.', '2019-07-07 10:08:09'),
(119, 'RainLab.Translate', 'comment', '1.1.4', 'Adds new multilingual markdown editor.', '2019-07-07 10:08:09'),
(120, 'RainLab.Translate', 'comment', '1.1.5', 'Minor update to the multilingual control API.', '2019-07-07 10:08:09'),
(121, 'RainLab.Translate', 'comment', '1.1.6', 'Minor improvements in the message editor.', '2019-07-07 10:08:09'),
(122, 'RainLab.Translate', 'comment', '1.1.7', 'Fixes bug not showing content when first loading multilingual textarea controls.', '2019-07-07 10:08:09'),
(123, 'RainLab.Translate', 'comment', '1.2.0', 'CMS pages now support translating the URL.', '2019-07-07 10:08:09'),
(124, 'RainLab.Translate', 'comment', '1.2.1', 'Minor update in the rich editor and code editor language control position.', '2019-07-07 10:08:09'),
(125, 'RainLab.Translate', 'comment', '1.2.2', 'Static Pages now support translating the URL.', '2019-07-07 10:08:09'),
(126, 'RainLab.Translate', 'comment', '1.2.3', 'Fixes Rich Editor when inserting a page link.', '2019-07-07 10:08:09'),
(127, 'RainLab.Translate', 'script', '1.2.4', 'create_indexes_table.php', '2019-07-07 10:08:09'),
(128, 'RainLab.Translate', 'comment', '1.2.4', 'Translatable attributes can now be declared as indexes.', '2019-07-07 10:08:09'),
(129, 'RainLab.Translate', 'comment', '1.2.5', 'Adds new multilingual repeater form widget.', '2019-07-07 10:08:09'),
(130, 'RainLab.Translate', 'comment', '1.2.6', 'Fixes repeater usage with static pages plugin.', '2019-07-07 10:08:09'),
(131, 'RainLab.Translate', 'comment', '1.2.7', 'Fixes placeholder usage with static pages plugin.', '2019-07-07 10:08:09'),
(132, 'RainLab.Translate', 'comment', '1.2.8', 'Improvements to code for latest October build compatibility.', '2019-07-07 10:08:09'),
(133, 'RainLab.Translate', 'comment', '1.2.9', 'Fixes context for translated strings when used with Static Pages.', '2019-07-07 10:08:09'),
(134, 'RainLab.Translate', 'comment', '1.2.10', 'Minor UI fix to the multilingual repeater.', '2019-07-07 10:08:09'),
(135, 'RainLab.Translate', 'comment', '1.2.11', 'Fixes translation not working with partials loaded via AJAX.', '2019-07-07 10:08:09'),
(136, 'RainLab.Translate', 'comment', '1.2.12', 'Add support for translating the new grouped repeater feature.', '2019-07-07 10:08:09'),
(137, 'RainLab.Translate', 'comment', '1.3.0', 'Added search to the translate messages page.', '2019-07-07 10:08:09'),
(138, 'RainLab.Translate', 'script', '1.3.1', 'builder_table_update_rainlab_translate_locales.php', '2019-07-07 10:08:09'),
(139, 'RainLab.Translate', 'script', '1.3.1', 'seed_all_tables.php', '2019-07-07 10:08:09'),
(140, 'RainLab.Translate', 'comment', '1.3.1', 'Added reordering to languages', '2019-07-07 10:08:09'),
(141, 'RainLab.Translate', 'comment', '1.3.2', 'Improved compatibility with RainLab.Pages, added ability to scan Mail Messages for translatable variables.', '2019-07-07 10:08:09'),
(142, 'RainLab.Translate', 'comment', '1.3.3', 'Fix to the locale picker session handling in Build 420 onwards.', '2019-07-07 10:08:09'),
(143, 'RainLab.Translate', 'comment', '1.3.4', 'Add alternate hreflang elements and adds prefixDefaultLocale setting.', '2019-07-07 10:08:09'),
(144, 'RainLab.Translate', 'comment', '1.3.5', 'Fix MLRepeater bug when switching locales.', '2019-07-07 10:08:09'),
(145, 'RainLab.Translate', 'comment', '1.3.6', 'Fix Middleware to use the prefixDefaultLocale setting introduced in 1.3.4', '2019-07-07 10:08:09'),
(146, 'RainLab.Translate', 'comment', '1.3.7', 'Fix config reference in LocaleMiddleware', '2019-07-07 10:08:09'),
(147, 'RainLab.Translate', 'comment', '1.3.8', 'Keep query string when switching locales', '2019-07-07 10:08:09'),
(148, 'RainLab.Translate', 'comment', '1.4.0', 'Add importer and exporter for messages', '2019-07-07 10:08:09'),
(149, 'RainLab.Translate', 'comment', '1.4.1', 'Updated Hungarian translation. Added Arabic translation. Fixed issue where default texts are overwritten by import. Fixed issue where the language switcher for repeater fields would overlap with the first repeater row.', '2019-07-07 10:08:09'),
(150, 'RainLab.Translate', 'comment', '1.4.2', 'Add multilingual MediaFinder', '2019-07-07 10:08:09'),
(151, 'RainLab.Translate', 'comment', '1.4.3', '!!! Please update OctoberCMS to Build 444 before updating this plugin. Added ability to translate CMS Pages fields (e.g. title, description, meta-title, meta-description)', '2019-07-07 10:08:09'),
(152, 'RainLab.Translate', 'comment', '1.4.4', 'Minor improvements to compatibility with Laravel framework.', '2019-07-07 10:08:09'),
(153, 'RainLab.Translate', 'comment', '1.4.5', 'Fixed issue when using the language switcher', '2019-07-07 10:08:09'),
(154, 'RainLab.Translate', 'comment', '1.5.0', 'Compatibility fix with Build 451', '2019-07-07 10:08:09'),
(155, 'RainLab.Translate', 'comment', '1.6.0', 'Make File Upload widget properties translatable. Merge Repeater core changes into MLRepeater widget. Add getter method to retrieve original translate data.', '2019-07-07 10:08:09'),
(156, 'Alipo.Page', 'script', '1.0.3', 'create_contacts_table.php', '2019-07-08 20:06:40'),
(157, 'Alipo.Page', 'script', '1.0.3', 'create_facilities_table.php', '2019-07-08 20:06:40'),
(158, 'Alipo.Page', 'script', '1.0.3', 'create_homes_table.php', '2019-07-08 20:06:40'),
(159, 'Alipo.Page', 'script', '1.0.3', 'create_introductions_table.php', '2019-07-08 20:06:40'),
(160, 'Alipo.Page', 'script', '1.0.3', 'create_locations_table.php', '2019-07-08 20:06:40'),
(161, 'Alipo.Page', 'script', '1.0.3', 'create_news_table.php', '2019-07-08 20:06:40'),
(162, 'Alipo.Page', 'script', '1.0.3', 'create_type_of_houses_table.php', '2019-07-08 20:06:40'),
(163, 'Alipo.Page', 'script', '1.0.3', 'create_facility_details_table.php', '2019-07-08 20:06:40'),
(164, 'Alipo.Project', 'script', '1.0.3', 'create_projects_table.php', '2019-07-09 00:20:02'),
(165, 'Alipo.Project', 'script', '1.0.4', 'create_projects_table.php', '2019-07-15 17:37:30'),
(166, 'Alipo.Page', 'script', '1.0.4', 'create_contacts_table.php', '2019-07-27 22:05:14'),
(167, 'Alipo.Page', 'script', '1.0.4', 'create_facilities_table.php', '2019-07-27 22:05:14'),
(168, 'Alipo.Page', 'script', '1.0.4', 'create_homes_table.php', '2019-07-27 22:05:14'),
(169, 'Alipo.Page', 'script', '1.0.4', 'create_introductions_table.php', '2019-07-27 22:05:14'),
(170, 'Alipo.Page', 'script', '1.0.4', 'create_locations_table.php', '2019-07-27 22:05:14'),
(171, 'Alipo.Page', 'script', '1.0.4', 'create_news_table.php', '2019-07-27 22:05:14'),
(172, 'Alipo.Page', 'script', '1.0.4', 'create_type_of_houses_table.php', '2019-07-27 22:05:14'),
(173, 'Alipo.Page', 'script', '1.0.4', 'create_facility_details_table.php', '2019-07-27 22:05:14'),
(174, 'Alipo.Page', 'script', '1.0.4', 'create_processes_table.php', '2019-07-27 22:05:14'),
(175, 'Alipo.Page', 'script', '1.0.6', 'create_contacts_table.php', '2019-08-24 11:53:54'),
(176, 'Alipo.Page', 'script', '1.0.6', 'create_facilities_table.php', '2019-08-24 11:53:54'),
(177, 'Alipo.Page', 'script', '1.0.6', 'create_homes_table.php', '2019-08-24 11:53:54'),
(178, 'Alipo.Page', 'script', '1.0.6', 'create_introductions_table.php', '2019-08-24 11:53:54'),
(179, 'Alipo.Page', 'script', '1.0.6', 'create_locations_table.php', '2019-08-24 11:53:54'),
(180, 'Alipo.Page', 'script', '1.0.6', 'create_news_table.php', '2019-08-24 11:53:54'),
(181, 'Alipo.Page', 'script', '1.0.6', 'create_type_of_houses_table.php', '2019-08-24 11:53:54'),
(182, 'Alipo.Page', 'script', '1.0.6', 'create_facility_details_table.php', '2019-08-24 11:53:54'),
(183, 'Alipo.Page', 'script', '1.0.6', 'create_processes_table.php', '2019-08-24 11:53:54'),
(184, 'Alipo.Page', 'script', '1.0.6', 'create_galleries_table.php', '2019-08-24 11:53:54'),
(185, 'Alipo.Page', 'script', '1.0.6', 'create_chu_dau_tus_table.php', '2019-08-24 11:53:54'),
(186, 'Alipo.Page', 'script', '1.0.8', 'create_contacts_table.php', '2019-08-24 11:55:21'),
(187, 'Alipo.Page', 'script', '1.0.8', 'create_facilities_table.php', '2019-08-24 11:55:21'),
(188, 'Alipo.Page', 'script', '1.0.8', 'create_homes_table.php', '2019-08-24 11:55:21'),
(189, 'Alipo.Page', 'script', '1.0.8', 'create_introductions_table.php', '2019-08-24 11:55:21'),
(190, 'Alipo.Page', 'script', '1.0.8', 'create_locations_table.php', '2019-08-24 11:55:21'),
(191, 'Alipo.Page', 'script', '1.0.8', 'create_news_table.php', '2019-08-24 11:55:21'),
(192, 'Alipo.Page', 'script', '1.0.8', 'create_type_of_houses_table.php', '2019-08-24 11:55:21'),
(193, 'Alipo.Page', 'script', '1.0.8', 'create_facility_details_table.php', '2019-08-24 11:55:21'),
(194, 'Alipo.Page', 'script', '1.0.8', 'create_processes_table.php', '2019-08-24 11:55:21'),
(195, 'Alipo.Page', 'script', '1.0.8', 'create_galleries_table.php', '2019-08-24 11:55:21'),
(196, 'Alipo.Page', 'script', '1.0.8', 'create_chu_dau_tus_table.php', '2019-08-24 11:55:21'),
(197, 'Alipo.Project', 'script', '1.0.8', 'create_projects_table.php', '2019-08-24 11:55:21'),
(198, 'Alipo.Page', 'script', '1.0.9', 'create_contacts_table.php', '2019-08-25 02:08:22'),
(199, 'Alipo.Page', 'script', '1.0.9', 'create_facilities_table.php', '2019-08-25 02:08:22'),
(200, 'Alipo.Page', 'script', '1.0.9', 'create_homes_table.php', '2019-08-25 02:08:22'),
(201, 'Alipo.Page', 'script', '1.0.9', 'create_introductions_table.php', '2019-08-25 02:08:22'),
(202, 'Alipo.Page', 'script', '1.0.9', 'create_locations_table.php', '2019-08-25 02:08:22'),
(203, 'Alipo.Page', 'script', '1.0.9', 'create_news_table.php', '2019-08-25 02:08:22'),
(204, 'Alipo.Page', 'script', '1.0.9', 'create_type_of_houses_table.php', '2019-08-25 02:08:22'),
(205, 'Alipo.Page', 'script', '1.0.9', 'create_facility_details_table.php', '2019-08-25 02:08:22'),
(206, 'Alipo.Page', 'script', '1.0.9', 'create_processes_table.php', '2019-08-25 02:08:22'),
(207, 'Alipo.Page', 'script', '1.0.9', 'create_galleries_table.php', '2019-08-25 02:08:22'),
(208, 'Alipo.Page', 'script', '1.0.9', 'create_chu_dau_tus_table.php', '2019-08-25 02:08:22'),
(209, 'Alipo.Page', 'script', '1.1.0', 'create_contacts_table.php', '2019-09-01 23:27:44'),
(210, 'Alipo.Page', 'script', '1.1.0', 'create_facilities_table.php', '2019-09-01 23:27:44'),
(211, 'Alipo.Page', 'script', '1.1.0', 'create_homes_table.php', '2019-09-01 23:27:44'),
(212, 'Alipo.Page', 'script', '1.1.0', 'create_introductions_table.php', '2019-09-01 23:27:44'),
(213, 'Alipo.Page', 'script', '1.1.0', 'create_locations_table.php', '2019-09-01 23:27:44'),
(214, 'Alipo.Page', 'script', '1.1.0', 'create_news_table.php', '2019-09-01 23:27:44'),
(215, 'Alipo.Page', 'script', '1.1.0', 'create_type_of_houses_table.php', '2019-09-01 23:27:44'),
(216, 'Alipo.Page', 'script', '1.1.0', 'create_facility_details_table.php', '2019-09-01 23:27:44'),
(217, 'Alipo.Page', 'script', '1.1.0', 'create_processes_table.php', '2019-09-01 23:27:44'),
(218, 'Alipo.Page', 'script', '1.1.0', 'create_galleries_table.php', '2019-09-01 23:27:44'),
(219, 'Alipo.Page', 'script', '1.1.0', 'create_chu_dau_tus_table.php', '2019-09-01 23:27:44'),
(220, 'Alipo.Page', 'script', '1.1.1', 'create_contacts_table.php', '2019-09-11 10:49:09'),
(221, 'Alipo.Page', 'script', '1.1.1', 'create_facilities_table.php', '2019-09-11 10:49:09'),
(222, 'Alipo.Page', 'script', '1.1.1', 'create_homes_table.php', '2019-09-11 10:49:09'),
(223, 'Alipo.Page', 'script', '1.1.1', 'create_introductions_table.php', '2019-09-11 10:49:09'),
(224, 'Alipo.Page', 'script', '1.1.1', 'create_locations_table.php', '2019-09-11 10:49:09'),
(225, 'Alipo.Page', 'script', '1.1.1', 'create_news_table.php', '2019-09-11 10:49:09'),
(226, 'Alipo.Page', 'script', '1.1.1', 'create_type_of_houses_table.php', '2019-09-11 10:49:09'),
(227, 'Alipo.Page', 'script', '1.1.1', 'create_facility_details_table.php', '2019-09-11 10:49:09'),
(228, 'Alipo.Page', 'script', '1.1.1', 'create_processes_table.php', '2019-09-11 10:49:09'),
(229, 'Alipo.Page', 'script', '1.1.1', 'create_galleries_table.php', '2019-09-11 10:49:09'),
(230, 'Alipo.Page', 'script', '1.1.1', 'create_chu_dau_tus_table.php', '2019-09-11 10:49:09'),
(231, 'Alipo.Page', 'script', '1.1.2', 'create_contacts_table.php', '2019-09-11 10:49:32'),
(232, 'Alipo.Page', 'script', '1.1.2', 'create_facilities_table.php', '2019-09-11 10:49:32'),
(233, 'Alipo.Page', 'script', '1.1.2', 'create_homes_table.php', '2019-09-11 10:49:32'),
(234, 'Alipo.Page', 'script', '1.1.2', 'create_introductions_table.php', '2019-09-11 10:49:32'),
(235, 'Alipo.Page', 'script', '1.1.2', 'create_locations_table.php', '2019-09-11 10:49:32'),
(236, 'Alipo.Page', 'script', '1.1.2', 'create_news_table.php', '2019-09-11 10:49:32'),
(237, 'Alipo.Page', 'script', '1.1.2', 'create_type_of_houses_table.php', '2019-09-11 10:49:32'),
(238, 'Alipo.Page', 'script', '1.1.2', 'create_facility_details_table.php', '2019-09-11 10:49:32'),
(239, 'Alipo.Page', 'script', '1.1.2', 'create_processes_table.php', '2019-09-11 10:49:32'),
(240, 'Alipo.Page', 'script', '1.1.2', 'create_galleries_table.php', '2019-09-11 10:49:32'),
(241, 'Alipo.Page', 'script', '1.1.2', 'create_chu_dau_tus_table.php', '2019-09-11 10:49:32'),
(242, 'Alipo.Cms', 'script', '3.0.2', 'create_home_pages_table.php', '2019-09-22 08:04:12'),
(243, 'Alipo.Cms', 'script', '3.0.2', 'create_design_pages_table.php', '2019-09-22 08:04:13'),
(244, 'Alipo.Cms', 'script', '3.0.2', 'create_neighbourhood_pages_table.php', '2019-09-22 08:04:13'),
(245, 'Alipo.Cms', 'script', '3.0.2', 'create_unit2_bedrooms_table.php', '2019-09-22 08:04:13'),
(246, 'Alipo.Cms', 'script', '3.0.2', 'create_unit3_bedrooms_table.php', '2019-09-22 08:04:13'),
(247, 'Alipo.Cms', 'script', '3.0.2', 'create_unit4_bedrooms_table.php', '2019-09-22 08:04:13'),
(248, 'Alipo.Cms', 'script', '3.0.2', 'create_general_options_table.php', '2019-09-22 08:04:13'),
(249, 'Alipo.Cms', 'script', '3.0.2', 'create_contact_pages_table.php', '2019-09-22 08:04:13'),
(250, 'Alipo.Cms', 'script', '3.0.2', 'create_developer_pages_table.php', '2019-09-22 08:04:13'),
(251, 'Alipo.Cms', 'script', '3.0.2', 'create_duplexes_table.php', '2019-09-22 08:04:13'),
(252, 'Alipo.Cms', 'script', '3.0.2', 'create_media_pages_table.php', '2019-09-22 08:04:13'),
(253, 'Alipo.Cms', 'script', '3.0.2', 'create_penthouses_table.php', '2019-09-22 08:04:13'),
(254, 'Alipo.Cms', 'script', '3.0.2', 'create_service_pages_table.php', '2019-09-22 08:04:13'),
(255, 'Alipo.Cms', 'script', '3.0.2', 'create_duplex_as_table.php', '2019-09-22 08:04:13'),
(256, 'Alipo.Cms', 'script', '3.0.2', 'create_duplex_b1s_table.php', '2019-09-22 08:04:13'),
(257, 'Alipo.Cms', 'script', '3.0.2', 'create_duplex_bs_table.php', '2019-09-22 08:04:13'),
(258, 'Alipo.Cms', 'script', '3.0.2', 'create_penthouse_as_table.php', '2019-09-22 08:04:13'),
(259, 'Alipo.Cms', 'script', '3.0.2', 'create_penthouse_bs_table.php', '2019-09-22 08:04:13'),
(260, 'Alipo.Cms', 'script', '3.0.2', 'create_unit3_bedroom_type_as_table.php', '2019-09-22 08:04:13'),
(261, 'Alipo.Cms', 'script', '3.0.2', 'create_unit3_bedroom_type_bs_table.php', '2019-09-22 08:04:13'),
(262, 'Alipo.Cms', 'script', '3.0.2', 'create_unit3_bedroom_type_cs_table.php', '2019-09-22 08:04:13'),
(263, 'Utopigs.Seo', 'script', '1.0.1', 'create_data_table.php', '2019-09-22 08:04:14'),
(264, 'Utopigs.Seo', 'comment', '1.0.1', 'First version of Utopigs Seo plugin', '2019-09-22 08:04:14'),
(265, 'Utopigs.Seo', 'comment', '1.0.2', 'Fix bug with elements with nested items', '2019-09-22 08:04:14'),
(266, 'Utopigs.Seo', 'script', '1.1.0', 'create_sitemaps_table.php', '2019-09-22 08:04:14'),
(267, 'Utopigs.Seo', 'comment', '1.1.0', 'Add sitemap functionality', '2019-09-22 08:04:14'),
(268, 'Utopigs.Seo', 'comment', '1.1.0', 'Fix some bugs with nested items', '2019-09-22 08:04:14'),
(269, 'Utopigs.Seo', 'comment', '1.1.1', 'Fix browser render issue', '2019-09-22 08:04:14'),
(270, 'RainLab.Translate', 'comment', '1.6.1', 'Add ability for models to provide translated computed data, add option to disable locale prefix routing', '2019-09-22 08:07:49'),
(271, 'RainLab.Translate', 'comment', '1.6.2', 'Implement localeUrl filter, add per-locale theme configuration support', '2019-09-22 08:07:49'),
(272, 'RainLab.GoogleAnalytics', 'comment', '1.0.1', 'Initialize plugin', '2019-09-22 20:20:53'),
(273, 'RainLab.GoogleAnalytics', 'comment', '1.0.2', 'Fixed a minor bug in the Top Pages widget', '2019-09-22 20:20:53'),
(274, 'RainLab.GoogleAnalytics', 'comment', '1.0.3', 'Minor improvements to the code', '2019-09-22 20:20:53'),
(275, 'RainLab.GoogleAnalytics', 'comment', '1.0.4', 'Fixes a bug where the certificate upload fails', '2019-09-22 20:20:53'),
(276, 'RainLab.GoogleAnalytics', 'comment', '1.0.5', 'Minor fix to support the updated Google Analytics library', '2019-09-22 20:20:53'),
(277, 'RainLab.GoogleAnalytics', 'comment', '1.0.6', 'Fixes dashboard widget using latest Google Analytics library', '2019-09-22 20:20:53'),
(278, 'RainLab.GoogleAnalytics', 'comment', '1.0.7', 'Removes Client ID from settings because the workflow no longer needs it', '2019-09-22 20:20:53'),
(279, 'RainLab.GoogleAnalytics', 'comment', '1.1.0', '!!! Updated to the latest Google API library', '2019-09-22 20:20:53'),
(280, 'RainLab.GoogleAnalytics', 'comment', '1.2.0', 'Update Guzzle library to version 6.0', '2019-09-22 20:20:53'),
(281, 'RainLab.GoogleAnalytics', 'comment', '1.2.1', 'Update the plugin compatibility with RC8 Google API client', '2019-09-22 20:20:53'),
(282, 'RainLab.GoogleAnalytics', 'comment', '1.2.2', 'Improve translations, bump version requirement to PHP 7', '2019-09-22 20:20:53'),
(283, 'RainLab.GoogleAnalytics', 'comment', '1.2.3', 'Added a switch for forceSSL', '2019-09-22 20:20:53'),
(284, 'RainLab.GoogleAnalytics', 'comment', '1.2.4', 'Added permission for dashboard widgets. Added Turkish, Spanish and Estonian translations.', '2019-09-22 20:20:53'),
(285, 'Alipo.GeneralOption', 'script', '1.0.2', 'create_generals_table.php', '2019-10-26 21:10:09'),
(286, 'Alipo.GeneralOption', 'script', '1.0.2', 'create_contact_infos_table.php', '2019-10-26 21:10:09'),
(287, 'Utopigs.Seo', 'comment', '1.1.2', 'Undo last change to fix Google Search Console sitemap fetch issues', '2019-10-26 21:10:09'),
(288, 'Utopigs.Seo', 'comment', '1.1.3', 'Add a sitemap-debug.xml that renders ok (using https protocol for the namespace attributes) to be able to debug sitemap issues visually', '2019-10-26 21:10:09'),
(289, 'Utopigs.Seo', 'comment', '1.1.4', 'Fix bug model image not showing', '2019-10-26 21:10:09'),
(290, 'Utopigs.Seo', 'comment', '1.1.5', 'Fix bug with nested categories', '2019-10-26 21:10:09'),
(291, 'Utopigs.Seo', 'comment', '1.1.6', 'Try to retrieve image from defaults if not specified', '2019-10-26 21:10:09'),
(292, 'Utopigs.Seo', 'comment', '1.1.6', 'Autofill properties for blog post and category pages', '2019-10-26 21:10:09'),
(293, 'Alipo.GeneralOption', 'script', '1.0.3', 'create_generals_table.php', '2019-10-26 21:35:51');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_versions`
--

CREATE TABLE `system_plugin_versions` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_frozen` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_versions`
--

INSERT INTO `system_plugin_versions` (`id`, `code`, `version`, `created_at`, `is_disabled`, `is_frozen`) VALUES
(1, 'BenFreke.MenuManager', '1.5.3', '2019-07-07 10:08:08', 0, 0),
(2, 'October.Demo', '1.0.1', '2019-07-07 10:08:08', 0, 0),
(3, 'Alipo.Page', '1.1.2', '2019-09-11 10:49:32', 0, 0),
(4, 'Alipo.Post', '1.0.1', '2019-07-07 10:08:09', 0, 0),
(5, 'Alipo.GeneralOption', '1.0.2', '2019-10-26 21:10:09', 0, 0),
(6, 'Alipo.Project', '1.0.8', '2019-08-24 11:55:21', 0, 0),
(7, 'LaminSanneh.FlexiContact', '1.3.4', '2019-07-07 10:08:09', 0, 0),
(8, 'AnandPatel.WysiwygEditors', '1.2.9', '2019-07-07 10:08:09', 0, 0),
(9, 'RainLab.Translate', '1.6.2', '2019-09-22 08:07:49', 0, 0),
(10, 'Alipo.Cms', '3.0.2', '2019-09-22 08:04:13', 0, 0),
(11, 'Utopigs.Seo', '1.1.6', '2019-10-26 21:10:09', 0, 0),
(12, 'RainLab.GoogleAnalytics', '1.2.4', '2019-09-22 20:20:53', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `system_request_logs`
--

CREATE TABLE `system_request_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` text COLLATE utf8mb4_unicode_ci,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_revisions`
--

CREATE TABLE `system_revisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci,
  `new_value` text COLLATE utf8mb4_unicode_ci,
  `revisionable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

CREATE TABLE `system_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_settings`
--

INSERT INTO `system_settings` (`id`, `item`, `value`) VALUES
(1, 'laminsanneh_flexicontact_settings', '{\"recipient_email\":\"info@bbsmartdecor.com\",\"recipient_name\":\"BB Smart Decor\",\"subject\":\"Mail from BB Smart Decor\",\"confirmation_text\":\"Thanks for your contact!\",\"enable_captcha\":\"0\",\"site_key\":\"\",\"secret_key\":\"\",\"enable_server_captcha_validation\":\"0\"}'),
(2, 'system_mail_settings', '{\"send_mode\":\"smtp\",\"sender_name\":\"BB Smart Decor\",\"sender_email\":\"dovietliemtv@gmail.com\",\"sendmail_path\":\"\\/usr\\/sbin\\/sendmail -bs\",\"smtp_address\":\"smtp.gmail.com\",\"smtp_port\":\"465\",\"smtp_user\":\"dovietliemtv@gmail.com\",\"smtp_password\":\"axzhtjeqzzwhxrjq\",\"smtp_authorization\":\"1\",\"smtp_encryption\":\"ssl\",\"mailgun_domain\":\"\",\"mailgun_secret\":\"\",\"mandrill_secret\":\"\",\"ses_key\":\"\",\"ses_secret\":\"\",\"ses_region\":\"\"}'),
(3, 'backend_brand_settings', '{\"app_name\":\"Stella Megacity\",\"app_tagline\":\"Stella Megacity\",\"primary_color\":\"#34495e\",\"secondary_color\":\"#e67e22\",\"accent_color\":\"#3498db\",\"menu_mode\":\"inline\",\"custom_css\":\"\"}'),
(4, 'cms_maintenance_settings', '{\"is_enabled\":\"0\",\"cms_page\":\"types-houses-detail.htm\",\"theme_map\":{\"cantho\":\"types-houses-detail.htm\"}}');

-- --------------------------------------------------------

--
-- Table structure for table `utopigs_seo_data`
--

CREATE TABLE `utopigs_seo_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utopigs_seo_sitemaps`
--

CREATE TABLE `utopigs_seo_sitemaps` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alipo_cms_general_options`
--
ALTER TABLE `alipo_cms_general_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_home_pages`
--
ALTER TABLE `alipo_cms_home_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_media_pages`
--
ALTER TABLE `alipo_cms_media_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_neighbourhood_pages`
--
ALTER TABLE `alipo_cms_neighbourhood_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_generaloption_contact_infos`
--
ALTER TABLE `alipo_generaloption_contact_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_generaloption_generals`
--
ALTER TABLE `alipo_generaloption_generals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_page_galleries`
--
ALTER TABLE `alipo_page_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_page_homes`
--
ALTER TABLE `alipo_page_homes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_users`
--
ALTER TABLE `backend_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login_unique` (`login`),
  ADD UNIQUE KEY `email_unique` (`email`),
  ADD KEY `act_code_index` (`activation_code`),
  ADD KEY `reset_code_index` (`reset_password_code`),
  ADD KEY `admin_role_index` (`role_id`);

--
-- Indexes for table `backend_users_groups`
--
ALTER TABLE `backend_users_groups`
  ADD PRIMARY KEY (`user_id`,`user_group_id`);

--
-- Indexes for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_unique` (`name`),
  ADD KEY `code_index` (`code`);

--
-- Indexes for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`);

--
-- Indexes for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_unique` (`name`),
  ADD KEY `role_code_index` (`code`);

--
-- Indexes for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `backend_user_throttle_user_id_index` (`user_id`),
  ADD KEY `backend_user_throttle_ip_address_index` (`ip_address`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_data_theme_index` (`theme`);

--
-- Indexes for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_logs_type_index` (`type`),
  ADD KEY `cms_theme_logs_theme_index` (`theme`),
  ADD KEY `cms_theme_logs_user_id_index` (`user_id`);

--
-- Indexes for table `cms_theme_templates`
--
ALTER TABLE `cms_theme_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_templates_source_index` (`source`),
  ADD KEY `cms_theme_templates_path_index` (`path`);

--
-- Indexes for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deferred_bindings_master_type_index` (`master_type`),
  ADD KEY `deferred_bindings_master_field_index` (`master_field`),
  ADD KEY `deferred_bindings_slave_type_index` (`slave_type`),
  ADD KEY `deferred_bindings_slave_id_index` (`slave_id`),
  ADD KEY `deferred_bindings_session_key_index` (`session_key`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rainlab_translate_attributes`
--
ALTER TABLE `rainlab_translate_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_attributes_locale_index` (`locale`),
  ADD KEY `rainlab_translate_attributes_model_id_index` (`model_id`),
  ADD KEY `rainlab_translate_attributes_model_type_index` (`model_type`);

--
-- Indexes for table `rainlab_translate_indexes`
--
ALTER TABLE `rainlab_translate_indexes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_indexes_locale_index` (`locale`),
  ADD KEY `rainlab_translate_indexes_model_id_index` (`model_id`),
  ADD KEY `rainlab_translate_indexes_model_type_index` (`model_type`),
  ADD KEY `rainlab_translate_indexes_item_index` (`item`);

--
-- Indexes for table `rainlab_translate_locales`
--
ALTER TABLE `rainlab_translate_locales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_locales_code_index` (`code`),
  ADD KEY `rainlab_translate_locales_name_index` (`name`);

--
-- Indexes for table `rainlab_translate_messages`
--
ALTER TABLE `rainlab_translate_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_messages_code_index` (`code`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_event_logs_level_index` (`level`);

--
-- Indexes for table `system_files`
--
ALTER TABLE `system_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_files_field_index` (`field`),
  ADD KEY `system_files_attachment_id_index` (`attachment_id`),
  ADD KEY `system_files_attachment_type_index` (`attachment_type`);

--
-- Indexes for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_mail_templates_layout_id_index` (`layout_id`);

--
-- Indexes for table `system_parameters`
--
ALTER TABLE `system_parameters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_index` (`namespace`,`group`,`item`);

--
-- Indexes for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_history_code_index` (`code`),
  ADD KEY `system_plugin_history_type_index` (`type`);

--
-- Indexes for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_versions_code_index` (`code`);

--
-- Indexes for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_revisions`
--
ALTER TABLE `system_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`),
  ADD KEY `system_revisions_user_id_index` (`user_id`),
  ADD KEY `system_revisions_field_index` (`field`);

--
-- Indexes for table `system_settings`
--
ALTER TABLE `system_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_settings_item_index` (`item`);

--
-- Indexes for table `utopigs_seo_data`
--
ALTER TABLE `utopigs_seo_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `utopigs_seo_sitemaps`
--
ALTER TABLE `utopigs_seo_sitemaps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `utopigs_seo_sitemaps_theme_index` (`theme`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alipo_cms_general_options`
--
ALTER TABLE `alipo_cms_general_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_home_pages`
--
ALTER TABLE `alipo_cms_home_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_media_pages`
--
ALTER TABLE `alipo_cms_media_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_neighbourhood_pages`
--
ALTER TABLE `alipo_cms_neighbourhood_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_generaloption_contact_infos`
--
ALTER TABLE `alipo_generaloption_contact_infos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `alipo_generaloption_generals`
--
ALTER TABLE `alipo_generaloption_generals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_page_galleries`
--
ALTER TABLE `alipo_page_galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_page_homes`
--
ALTER TABLE `alipo_page_homes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `backend_users`
--
ALTER TABLE `backend_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_theme_templates`
--
ALTER TABLE `cms_theme_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `rainlab_translate_attributes`
--
ALTER TABLE `rainlab_translate_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `rainlab_translate_indexes`
--
ALTER TABLE `rainlab_translate_indexes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_translate_locales`
--
ALTER TABLE `rainlab_translate_locales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rainlab_translate_messages`
--
ALTER TABLE `rainlab_translate_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `system_files`
--
ALTER TABLE `system_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;

--
-- AUTO_INCREMENT for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `system_parameters`
--
ALTER TABLE `system_parameters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=294;

--
-- AUTO_INCREMENT for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_revisions`
--
ALTER TABLE `system_revisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_settings`
--
ALTER TABLE `system_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `utopigs_seo_data`
--
ALTER TABLE `utopigs_seo_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `utopigs_seo_sitemaps`
--
ALTER TABLE `utopigs_seo_sitemaps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
