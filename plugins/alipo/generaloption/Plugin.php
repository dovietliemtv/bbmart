<?php namespace Alipo\GeneralOption;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * GeneralOption Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'GeneralOption',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.GeneralOption', 'generaloption', 'plugins/alipo/generaloption/partials/sidebar');

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Alipo\GeneralOption\Components\General' => 'general',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        // return []; // Remove this line to activate

        return [
            'alipo.generaloption.*' => [
                'tab' => 'GeneralOption',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'generaloption' => [
                'label'       => 'Export',
                'url'         => Backend::url('alipo/generaloption/contactinfo'),
                'icon'        => 'icon-cog',
                'permissions' => ['alipo.generaloption.*'],
                'order'       => 500,
                'sideMenu' => [
                    'contactinfo' => [
                        'label'       => 'Thông tin liên hệ',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/generaloption/contactinfo'),
                        'permissions' => ['alipo.generaloption.*'],
                        'group'       => 'General Option'
                    ],
                ]
            ],
        ];
    }
}
