<?php namespace Alipo\GeneralOption\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * General Back-end Controller
 */
class General extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Alipo.GeneralOption', 'generaloption', 'general');
    }
}
