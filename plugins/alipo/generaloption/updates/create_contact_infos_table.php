<?php namespace Alipo\GeneralOption\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateContactInfosTable extends Migration
{
    public function up()
    {
        Schema::create('alipo_generaloption_contact_infos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->text('email');
            $table->text('phone');
            $table->text('message');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('alipo_generaloption_contact_infos');
    }
}
