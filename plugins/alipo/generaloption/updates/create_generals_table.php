<?php namespace Alipo\GeneralOption\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGeneralsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_generaloption_generals')){ 
            Schema::create('alipo_generaloption_generals', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title1');
                $table->text('des1');
                $table->text('phone');
                $table->text('social_network');
                $table->text('title2');
                $table->text('des2');
                $table->text('copyright');
                $table->timestamps();
            });    
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_generaloption_generals');
    }
}
