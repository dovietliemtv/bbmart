<?php namespace Alipo\Page\Models;

use Model;

/**
 * Home Model
 */
class Home extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'alipo_page_homes';
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
        'title',
        'introduction_des',
        'facilities_des',
        'type_house_dinfo',
    ];
    public $rules = [
        'title' => 'required',
        'introduction_des' => 'required',
        'facilities_des' => 'required',
    ];
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    protected $jsonable = ['type_house_dinfo', 'lydoluachon'];

    
    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'video' => 'System\Models\File',
    ];
    public $attachMany = [
        'banner' => 'System\Models\File',
    ];
}
