<?php namespace Alipo\Page\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProcessesTable extends Migration
{
    public function up()
    {

        if(!Schema::hasTable('alipo_page_processes')){ 
            Schema::create('alipo_page_processes', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->text('slug');
                $table->text('info');
                $table->timestamps();
            });
        }


    }

    public function down()
    {
        Schema::dropIfExists('alipo_page_processes');
    }
}
