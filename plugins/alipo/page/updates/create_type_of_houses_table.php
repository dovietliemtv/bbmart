<?php namespace Alipo\Page\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTypeOfHousesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_page_type_of_houses')){ 

            Schema::create('alipo_page_type_of_houses', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->text('slug');
                $table->timestamps();
            });
    
        }

    }

    public function down()
    {
        Schema::dropIfExists('alipo_page_type_of_houses');
    }
}
