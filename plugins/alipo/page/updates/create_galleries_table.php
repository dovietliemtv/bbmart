<?php namespace Alipo\Page\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGalleriesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_page_galleries')){ 
            Schema::create('alipo_page_galleries', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->text('slug');
                $table->text('images');
                $table->text('videos');
                $table->timestamps();
            });

        }

    }

    public function down()
    {
        Schema::dropIfExists('alipo_page_galleries');
    }
}
