<?php namespace Alipo\Page\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateHomesTable extends Migration
{
    public function up()
    {

        if(!Schema::hasTable('alipo_page_homes')){ 
            Schema::create('alipo_page_homes', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->text('slug');
                $table->text('background');
                $table->text('introduction_des');
                $table->text('facilities_des');
                $table->text('lydoluachon');
                $table->text('type_house_dinfo');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_page_homes');
    }
}
