<?php namespace Alipo\Page\Components;

use Cms\Classes\ComponentBase;
use Alipo\Page\Models\Home as HomeModel;

class Home extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Home Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    public function onRun() {
        parent::onRun(); // TODO: Change the autogenerated stub
        $this->page['homepage'] = HomeModel::first();
    }
}
