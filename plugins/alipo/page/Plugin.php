<?php namespace Alipo\Page;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * Page Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Page',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.Page', 'page', 'plugins/alipo/page/partials/sidebar');

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Alipo\Page\Components\Home' => 'homePage',
            'Alipo\Page\Components\GalleryCp' => 'gallerycp',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        // return []; // Remove this line to activate

        return [
            'alipo.page.*' => [
                'tab' => 'Page',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'page' => [
                'label'       => 'Trang',
                'url'         => Backend::url('alipo/page/home/update/1'),
                'icon'        => 'icon-file-powerpoint-o',
                'permissions' => ['alipo.page.*'],
                'order'       => 500,
                'sideMenu' => [
                    'homepage' => [
                        'label'       => 'Trang chủ',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/page/home/update/1'),
                        'permissions' => ['alipo.page.page'],
                        'group'       => 'Pages'
                    ],
                    'gallery' => [
                        'label'       => 'Thư viện',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/page/gallery/update/1'),
                        'permissions' => ['alipo.page.page'],
                        'group'       => 'Pages'
                    ],
                
                ]
            ],
        ];
    }
}
