'use strict';

(function ($) {
    $(document).ready(function () {
        let $body = $('body');
        let $html = $('html');
        let $window = $(window).eq(0);
        let $document = $(document).eq(0);

        toggleMenu();

        // loading all images of the page
        $body.waitForImages(function () {
            setTimeout(function () {
                // $('.ct-b-loading-wrapper').fadeOut(700, function () {
                //     $(this).remove();
                //     $body.addClass('add-header');
                //     $body.addClass('add-bottom-navi');
                // });
            }, 700)
            // animation
        });

        // Show menu for mobile
        $('#icon-menu').click(function () {
            $('div.menu-wrap').addClass('show-menu');
        })

        // Close menu for mobile
        $('.btn-close').click(function () {
            $('div.menu-wrap').removeClass('show-menu');
        });

        //close popup
        $(' html').on('click', '.ct-close-popup-content', function (e) {
            e.preventDefault();
            $('.ct-popup-confirm').remove();
        })

        $('html').click(function (e) {
            if ($(e.target).parents('.ct-popup-content').length == 0) {
                $('.ct-popup-confirm').remove();
            }
        });


        // menu lang
        $('.js-toggle-lang').on('click', function (e) {
            $('header').toggleClass('open-lang');
        })

        $('html').click(function (e) {
            if ($(e.target).parents('.ct-lang').length == 0) {
                $('header').removeClass('open-lang');
            }
        });

        // hotspot click
        $('html').on('click', '.js-hotspot-click', function (e) {
            e.preventDefault();
            $('.ct-hotspot').removeClass('active');
            let data = $(this).data('hotspot');
            $('.open-popup-' + data).toggleClass('active');
        })



        $('.js-thamquan').on('click', function (e) {

            if (!$body.hasClass('homepage')) {
                $('html, body').animate({
                    scrollTop: $(".ct-thamquan").offset().top
                }, 500);
            }

        })

        $('.js-back-to-top').on('click', function (e) {
            if (!$body.hasClass('homepage')) {
                $('html, body').animate({
                    scrollTop: 0,
                }, 500);
            }
        })

        // Slick slider function for home page
        $('.ct-home-slider').slick({
            dots: false,
            infinite: true,
            autoplaySpeed: 3000,
            fade: true,
            cssEase: 'linear',
            autoplay: true,
            cssEase: 'ease-in-out',
            pauseOnHover: false,
        });

        $('.ct-slider').slick({
            dots: false,
            infinite: true,
            autoplaySpeed: 4000,
            fade: true,
            cssEase: 'linear',
            autoplay: true,
            cssEase: 'ease-in-out',
            pauseOnHover: false,
        });


        $('.ct-content-slider').slick({
            dots: false,
            infinite: true,
            draggable: false,
            nextArrow: '<button class="btn slider-right-btn"><i class="fa fa-angle-right"></i></button>',
            prevArrow: '<button class="btn slider-left-btn"><i class="fa fa-angle-left"></i></button>',
        });




        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav',
            draggable: false,
            swipeToSlide: false,
            touchMove: false,
            swipe: false,
        });
        $('.slider-nav').slick({
            asNavFor: '.slider-for',
            focusOnSelect: true,
            dots: false,
            infinite: true,
            slidesToShow: 6,
            slidesToScroll: 1,
            nextArrow: '<button class="btn slider-right-btn"><i class="fa fa-angle-right"></i></button>',
            prevArrow: '<button class="btn slider-left-btn"><i class="fa fa-angle-left"></i></button>',
            draggable: false,
            swipeToSlide: false,
            touchMove: false,
            swipe: false,
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 4,
                    }
                }
            ]
        });


        $('.slider-for-gallery').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav-gallery',
            draggable: false,
            swipeToSlide: false,
            touchMove: false,
            accessibility: false,
            swipe: false,
        });

        $('.slider-nav-gallery').slick({
            asNavFor: '.slider-for-gallery',
            focusOnSelect: true,
            dots: false,
            infinite: true,
            slidesToShow: 6,
            slidesToScroll: 1,
            nextArrow: '<button class="btn slider-right-btn"><i class="fa fa-angle-right"></i></button>',
            prevArrow: '<button class="btn slider-left-btn"><i class="fa fa-angle-left"></i></button>',
            draggable: false,
            swipeToSlide: false,
            touchMove: false,
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3,
                        draggable: false,
                        draggable: false,
                        swipeToSlide: false,
                        touchMove: false,
                        accessibility: false,
                        swipe: false,
                    }
                }
            ]
        });


        $('.slider-for-gallery-video').slick({
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav-gallery-video',
            draggable: false,
            swipeToSlide: false,
            touchMove: false,
            accessibility: false,
            swipe: false,
        });

        $('.slider-nav-gallery-video').slick({
            asNavFor: '.slider-for-gallery-video',
            focusOnSelect: true,
            dots: true,
            infinite: true,
            arrows: false,
            slidesToShow: 4,
            slidesToScroll: 2,
            draggable: false,
            swipeToSlide: false,
            touchMove: false,
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        draggable: false,
                        draggable: false,
                        swipeToSlide: false,
                        touchMove: false,
                        accessibility: false,
                        swipe: false,
                    }
                }
            ]
        });

        $('.ct-slide-gallery').slick({
            dots: true,
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: false,
            rows: 2,
            // draggable: false,
            // swipeToSlide: false,
            // touchMove: false,
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        rows: 2,
                        dots: true,
                        // draggable: false,
                        // swipeToSlide: false,
                        // touchMove: false,
                        accessibility: false,
                        // swipe: false,
                    }
                }
            ]
        });

        $('.slider-news').slick({
            dots: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            rows: 2,
            arrows: true,
            nextArrow: '<button class="btn slider-right-btn"><i class="fa fa-angle-right"></i></button>',
            prevArrow: '<button class="btn slider-left-btn"><i class="fa fa-angle-left"></i></button>',
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        centerPadding: '40px',
                        slidesToShow: 1,
                        arrows: false,
                        rows: 3,
                        dots: true,
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        centerPadding: '40px',
                        slidesToShow: 1,
                        arrows: false,
                        rows: 2,
                        dots: true,
                    }
                }
            ]
        });

        $('.ct-facility-list-slider').slick({
            dots: false,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 2,
            arrows: false,
            rows: 2,
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        arrows: false,
                        dots: true,
                        rows: 1,
                    }
                }
            ]
        });

        $('.ct-facilities-slider').slick({
            dots: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
        });


        $('#fullpage').fullpage({
            navigation: true,
            navigationPosition: 'left',
            lockAnchors: false,
            verticalCentered: false,
            scrollingSpeed: 1000,
            // scrollOverflow: true,
            // responsive: 1280,
            anchors: ['trangchu', 'lydoluachon', 'thuvien', 'lienhe', 'footer'],


            onLeave: function (origin, destination, direction) {

                var width = $window.width();


            },
        });
        // detect screen size
        $body.layoutController({
            onSmartphone: function () {
            },
            onTablet: function () {
            },
            onDesktop: function () {

            }
        });

        // popup img
        $('.js-img-popup').magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'image',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            zoom: {
                enabled: true,
                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out' // CSS transition
            }
        });

        //popup for video
        // $('.js-iframe').magnificPopup({
        //     delegate: 'a', // child items selector, by clicking on it popup will open
        //     type: 'iframe',
        //     gallery: {
        //         enabled: true,
        //         navigateByImgClick: true,
        //         preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        //     },
        //     zoom: {
        //         enabled: true,
        //         duration: 300, // duration of the effect, in milliseconds
        //         easing: 'ease-in-out' // CSS transition
        //     }
        // });


        $(document).ready(function () {
            $('.flexiContactForm button').prop('disabled', true);
            $('.flexiContactForm input').keyup(function () {
                if ($(".flexiContactForm input[name='name']").val() != ''
                    && $(".flexiContactForm input[name='phone']").val() != '') {
                    $('.flexiContactForm button').prop('disabled', false);
                } else {
                    $('.flexiContactForm button').prop('disabled', true);
                }
            });
        });



    });//end document ready


    function toggleMenu() {
        $('.js-toggle-menu').on('click', () => {
            $('body').toggleClass('show-menu');
        })
    }



})(jQuery);
